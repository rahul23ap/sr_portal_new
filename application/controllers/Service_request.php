<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'Front_master.php';
class Service_request extends Front_master
{
	public function __construct()
	{
		parent:: __construct();
        if ($this->session->userdata('user_id') == '' || $this->session->userdata('user_type') == 'admin')
        { 
            redirect(base_url());
            exit();
        }
    }

    public function index()
    {	
        $this->config->config["page_title"]= 'My Service Requests';
    	$data = array();
    	$this->get_header();
    	$user_id = $this->session->userdata('user_id');
    	$cond=array('isActive' =>1,'isDelete'=>0);
    	$data['location_data']=$this->master_model->get_record('vg_location_master',$cond);

    	$this->load->view('service-request/service-listing',$data);
    	$this->get_footer();
    }

    public function notificationcount()
    {
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        if($user_type == 'employee')
        {
            $query3 = "SELECT GROUP_CONCAT(sr_id) as sr_ids FROM vg_sr_master WHERE user_id = $user_id and isActive = 1 and isDelete = 0 ";
            $get_sr_ids = $this->master_model->custom_query($query3);
            
            if(!empty($get_sr_ids) && $get_sr_ids[0]->sr_ids != 0)
            {   
                $sr_ids =  $get_sr_ids[0]->sr_ids;
                $query = "SELECT * FROM vg_request_history WHERE sr_id In ($sr_ids) and employee_read_status = 1 and isActive = 1 and isDelete = 0 Order By history_id DESC ";
                $get_count = $this->master_model->custom_query($query);

                return $get_count;
            }
        }
        elseif(!empty($user_id) && $user_type == 'support')
        {
            $cond=array(
                'user_id'=>$user_id,
                'user_status' =>1,
                'isDelete'=>0,
            );
            $filed=array('category_id','service_request_id');
            $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);

            if(!empty($user_data_master))
            {
                $category_id = $user_data_master[0]->category_id;
                $service_request_id = $user_data_master[0]->service_request_id;
                $cond = " (category_id In ($category_id) AND service_request_id In ($service_request_id)) AND isActive = 1 and isDelete = 0";

                $query = "SELECT GROUP_CONCAT(sr_id) as sr_ids FROM vg_sr_master WHERE $cond ";
                $get_sr_ids = $this->master_model->custom_query($query);


                if(!empty($get_sr_ids) && $get_sr_ids[0]->sr_ids != 0)
                {   
                    $sr_ids =  $get_sr_ids[0]->sr_ids;
                    $query = "SELECT * FROM vg_request_history WHERE sr_id In ($sr_ids) and support_read_status = 1 and isActive = 1 and isDelete = 0 Order By history_id DESC";
                    $get_count = $this->master_model->custom_query($query);
                    return $get_count;
                }
            }
        }
    }

    public function updatenotificationcount()
    {
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        $count = '';
        if($user_type == 'employee')
        {
            $query3 = "SELECT GROUP_CONCAT(sr_id) as sr_ids FROM vg_sr_master WHERE user_id = $user_id and isActive = 1 and isDelete = 0 ";
            $get_sr_ids = $this->master_model->custom_query($query3);
            
            if(!empty($get_sr_ids) && $get_sr_ids[0]->sr_ids != 0)
            {   
                $sr_ids =  $get_sr_ids[0]->sr_ids;
                
                $data=array(
                    'employee_read_status'=>0,
                );
                $cond= "sr_id In ($sr_ids) and employee_read_status = 1 and isActive = 1 and isDelete = 0";
                $this->master_model->update_record('vg_request_history',$data,$cond);    



                /*$update_query = "UPDATE vg_request_history SET employee_read_status = 0 WHERE sr_id In ($sr_ids) and employee_read_status = 1 and isActive = 1 and isDelete = 0";
                $get_update = $this->master_model->custom_query($update_query);*/

                $query = "SELECT count(history_id) as total_count FROM vg_request_history WHERE sr_id In ($sr_ids) and employee_read_status = 1 and isActive = 1 and isDelete = 0 Order By history_id DESC ";
                $get_count = $this->master_model->custom_query($query);
                if(!empty($get_count) && $get_count[0]->total_count != 0)
                {
                    $count = $get_count[0]->total_count;
                }
            }
        }
        elseif(!empty($user_id) && $user_type == 'support')
        {
            $cond=array(
                'user_id'=>$user_id,
                'user_status' =>1,
                'isDelete'=>0,
            );
            $filed=array('category_id','service_request_id');
            $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);

            if(!empty($user_data_master))
            {
                $category_id = $user_data_master[0]->category_id;
                $service_request_id = $user_data_master[0]->service_request_id;
                $cond = " (category_id In ($category_id) AND service_request_id In ($service_request_id)) AND isActive = 1 and isDelete = 0";

                $query = "SELECT GROUP_CONCAT(sr_id) as sr_ids FROM vg_sr_master WHERE $cond ";
                $get_sr_ids = $this->master_model->custom_query($query);


                if(!empty($get_sr_ids) && $get_sr_ids[0]->sr_ids != 0)
                {   
                    $sr_ids =  $get_sr_ids[0]->sr_ids;
                    
                    $data=array(
                        'support_read_status'=>0,
                    );
                    $cond= "sr_id In ($sr_ids) and support_read_status = 1 and isActive = 1 and isDelete = 0";
                    $this->master_model->update_record('vg_request_history',$data,$cond);  
                    // $update_query = "UPDATE vg_request_history SET support_read_status = 0 WHERE sr_id In ($sr_ids) and support_read_status = 1 and isActive = 1 and isDelete = 0";
                    // $get_update = $this->master_model->custom_query($update_query);

                    $query = "SELECT count(history_id) as total_count FROM vg_request_history WHERE sr_id In ($sr_ids) and support_read_status = 1 and isActive = 1 and isDelete = 0 Order By history_id DESC";
                    $get_count = $this->master_model->custom_query($query);
                    if(!empty($get_count) && $get_count[0]->total_count != 0)
                    {
                        $count = $get_count[0]->total_count;
                    }
                }
            }
        }
        echo json_encode(array("result"=>"success","count"=>$count));
    }


    public function setting()
    {	
        $this->config->config["page_title"]= 'Settings';
    	$user_id = $this->session->userdata('user_id');

        $this->get_header();
        $this->form_validation->set_rules('first_name', 'Please enter user name.', 'required');
        $this->form_validation->set_rules('last_name', 'Please enter last name.', 'required');
        $this->form_validation->set_rules('user_name', 'Please enter user name.', 'required');
        $this->form_validation->set_rules('location_id', 'Please select location.', 'required');
        if(!$this->input->post('user_type_hid'))
        {
            $this->form_validation->set_rules('user_type', 'Please select user type.', 'required');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $cond=array('isActive' =>1,'isDelete'=>0);
            $data['category_data']=$this->master_model->get_record('vg_category',$cond);
            $data['service_request_data']=$this->master_model->get_record('vg_service_request',$cond);
            $data['location_data']=$this->master_model->get_record('vg_location_master',$cond);

            $cond=array('user_id'=>$user_id,'user_status' => 1,'isDelete'=>0);   
            $data['user_data']=$this->master_model->get_one_record('vg_user_master',$cond);
            $this->load->view('service-request/setting',$data);
        }
        else
        { 
            $dCreatedDate = date("Y-m-d H:i:s");
            $dModifyDate = date("Y-m-d H:i:s");
            $back_url = $this->input->post('back_url');
            if(!empty($user_id))
            {
                $data=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'emailid'=>$this->input->post('emailid'),
                    'location_id'=>$this->input->post('location_id'),
                    'user_type'=>$this->input->post('user_type_hid'),
                    'category_id'=>$this->input->post('category_id'),
                    'service_request_id'=>$this->input->post('service_request_id'),
                    'dModifyDate' =>$dModifyDate,
                );
                $cond=array(
                    'user_id'=>$user_id,
                );
                $this->master_model->update_record('vg_user_master',$data,$cond);
                if(!empty($back_url))
                {
                    $this->session->set_userdata(array('back_url'=>$back_url));
                }
                
                $this->session->set_flashdata('success_msg','Settings successfully updated.');
            }
            redirect($this->agent->referrer());           
        }
        $this->get_footer();
    }

    public function support_user()
    {
        $category_id = $this->input->post('category_id');
        $service_request_id = $this->input->post('service_request_id');

        $cond = "FIND_IN_SET ('".$category_id."',category_id) AND FIND_IN_SET ('".$service_request_id."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
        $query3 = "SELECT user_id,first_name,last_name FROM vg_user_master WHERE $cond ";
        $support_users = $this->master_model->custom_query($query3);

        $data = '';
        if(!empty($support_users))
        {
            $data .= '<option value="">Select Assign To</option>'; 
            foreach($support_users as $support_users_row)
            {
                $data .= '<option value="'.$support_users_row->user_id.'">'.$support_users_row->first_name.' '.$support_users_row->last_name.'</option>';
            } 
        }

        echo json_encode(array("result"=>"success","data"=>$data));
    }

    public function support_user_edit()
    {
        $assign_id = $this->input->post('assign_id');
        $category_id = $this->input->post('category_id');
        $service_request_id = $this->input->post('service_request_id');

        $cond = "FIND_IN_SET ('".$category_id."',category_id) AND FIND_IN_SET ('".$service_request_id."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
        $query3 = "SELECT user_id,first_name,last_name FROM vg_user_master WHERE $cond ";
        $support_users = $this->master_model->custom_query($query3);

        $data = '';
        if(!empty($support_users))
        {
            $data .= '<option value="">Select Assign To</option>'; 
            foreach($support_users as $support_users_row)
            {
                $select = '';
                if($support_users_row->user_id == $assign_id)
                {
                    $select = 'selected=selected';
                }
                $data .= '<option value="'.$support_users_row->user_id.'" '.$select.' >'.$support_users_row->first_name.' '.$support_users_row->last_name.'</option>';
            } 
        }

        echo json_encode(array("result"=>"success","data"=>$data));
    }

    public function get_employee_user()
    {
        $cond=array('user_status'=>1,'user_type'=>'employee','isDelete'=>0);
        $filed=array('user_id','first_name','last_name');
        $data_employee=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        return $data_employee;
    }

    public function get_sr_details($sr_id)
    {
        $cond=array('sr_id'=>$sr_id,'isActive'=>1,'isDelete'=>0);
        $sr_data =$this->master_model->get_one_record('vg_sr_master',$cond);
        return $sr_data;
    }

    public function create_request()
    {

        $this->config->config["page_title"]= 'Create New SR';
        $this->get_header();
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $this->form_validation->set_rules('name', 'Please enter name.', 'required');
        $this->form_validation->set_rules('emailid', 'Please enter email address.', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $cond=array(
                'user_id'=>$user_id,
                'user_status' =>1,
                'isDelete'=>0,
            );
            $filed=array('category_id');
            $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
            $q= '';
            if(!empty($user_data_master) && !empty($user_data_master[0]->category_id) && $user_type == 'support')
            {
                $category_id  = $user_data_master[0]->category_id;
                $q  = " AND category_id IN ($category_id) ";
            }
            $query = "SELECT * FROM vg_category WHERE isActive = 1 AND isDelete = 0 $q ";
            $data['category_data'] = $this->master_model->custom_query($query);
            
            $cond=array('isActive' =>1,'isDelete'=>0);
            $data['service_request_data']=$this->master_model->get_record('vg_service_request',$cond);
            $data['location_data']=$this->master_model->get_record('vg_location_master',$cond);
            
            $this->config->config["page_title"]= 'Create User';  
            if(!empty($user_id))
            {
                $cond=array('user_id'=>$user_id,'isDelete'=>0);
                $filed=array('first_name','last_name','emailid','location_id');
                $data['user_data']=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);

                $this->config->config["page_title"]= 'Edit Profile Details';  
            }
            $this->load->view('service-request/create-request',$data);
        }
        else 
        { 

            // echo "Nope";
            // exit();
            $dCreatedDate = date("Y-m-d H:i:s");
            $dModifyDate = date("Y-m-d H:i:s");

            // $category_id = '';
            // $service_request_id = '';
            // if(!empty($this->input->post('category_id')))
            // {   
            //     $category_id = implode(',', $this->input->post('category_id'));
            // }
            // if(!empty($this->input->post('service_request_id')))
            // {   
            //     $service_request_id = implode(',', $this->input->post('service_request_id'));
            // }
            //$due_date = '';
            $date = '';
            $due_date = Null;
            if(!empty($this->input->post('due_date')))
            {
                $date =  $this->input->post('due_date');
                $date = str_replace('-', '/', $date);
                $due_date = date('Y-m-d', strtotime($date));
            }

            $priority = '';
            if(!empty($this->input->post('priority')))
            {
            	$priority = $this->input->post('priority');
            }

            $requester_id = $user_id;
            if($user_type == 'support' && !empty($this->input->post('user_id_hid')))
            {
                $user_id = $this->input->post('user_id_hid');
            }

            $assign_id = 0;
            if(!empty($this->input->post('assign_id')))
            {
                $assign_id = $this->input->post('assign_id');
            }
           
            $data=array(
                'user_id'=>$user_id,
                'assign_id'=>$assign_id,
                'requester_id'=>$requester_id,
                'name'=>$this->input->post('name'),
                'emailid'=>$this->input->post('emailid'),
                'category_id' =>$this->input->post('category_id_hid'),
                'service_request_id' =>$this->input->post('service_request_id'),
                'location_id'=>$this->input->post('location_id'),
                'requester_type'=>$this->input->post('user_type_hid'),
                'priority'=>$priority,
                'due_date'=>$due_date,
                'sr_title'=>$this->input->post('sr_title'),
                'sr_description'=>$this->input->post('sr_description'),
                'sr_status'=>'Open',
                'dCreatedDate' =>$dCreatedDate,
                'dModifyDate' =>$dModifyDate,
            );
           	$sr_id = $this->master_model->insert_record('vg_sr_master',$data);


            if (!empty($_FILES['attachment_file']['name']))
            {
               foreach ($_FILES['attachment_file']["name"] as $file=>$key) 
               {
                    if (!empty($_FILES['attachment_file']["name"][$file])) 
                    {
                        $file_name = $_FILES['attachment_file']["name"][$file];
                        $cond = array("sattachment"=>$file_name,'isDelete' => 0);
                        $rec = $this->master_model->get_record('vg_sr_attachment',$cond);
                        $results = count($rec);

                        if($results > 0)
                        {
                            $file_name = date('m-d-Y-h-i-s').'-'.$file_name;
                        }

                        $_FILES['nsImage'] ["name"] = $file_name;
                        $_FILES['nsImage'] ["type"] = $_FILES['attachment_file']["type"][$file];
                        $_FILES['nsImage'] ["tmp_name"] = $_FILES['attachment_file']["tmp_name"][$file];
                        $_FILES['nsImage'] ["error"] = $_FILES['attachment_file']["error"][$file];
                        $_FILES['nsImage'] ["size"] = $_FILES['attachment_file']["size"][$file];


                        $photo=$this->do_upload('nsImage','sr_documents');
                        // var_dump($photo);
                        // exit();
                        $data=array(
                            'user_id'=>$user_id,
                            'sr_id'=>$sr_id,
                            'sattachment' =>$photo['upload_data']['file_name'],
                            'dCreatedDate' => $dCreatedDate,
                        );
                        $this->master_model->insert_record('vg_sr_attachment',$data);
                    }
                }
            }

            $sr_data = $this->get_sr_details($sr_id);

            $assign_id = 0;
            $assign_name = '';
            if(!empty($sr_data) && $sr_data->assign_id != 0)
            {
                $assign_id = $sr_data->assign_id;
                $assign_name = $this->get_assign_name($sr_data->assign_id);
            }

            $employee_read_status = 0;
            $support_read_status = 0;
            if($user_type == 'support')
            {
                $employee_read_status = 1;
            }
            elseif($user_type == 'employee')
            {
                $support_read_status = 1;
            }

            $activity_by = $this->session->userdata('user_id');
                        
            $data=array(
                'sr_id'=>$sr_id,
                'user_id'=>$user_id,
                'user_type'=>$user_type,
                'assign_id'=>$assign_id,
                'sr_status' =>'Open',
                'activity_by'=>$activity_by,
                'activity_type'=>'Request Opened',
                'employee_read_status'=>$employee_read_status,
                'support_read_status'=>$support_read_status,
                'dCreatedDate' => $dCreatedDate,
            );
           	$this->master_model->insert_record('vg_request_history',$data);


            $category_name =  $this->get_category($this->input->post('category_id_hid'));
            $service_request_name =  $this->get_srtype($this->input->post('service_request_id'));
            $location_name =   $this->get_location($this->input->post('location_id'));

            $viewData['mail_data']=array(
                'user_type'=>$user_type,
                'email_title'=>'New Service Request',
                'email_text'=>'Your service request is created successfully. Please find your service request details below.',
                'name'=>$this->input->post('name'),
                'emailid'=>$this->input->post('emailid'),
                'category_name' =>$category_name,
                'service_request_name' =>$service_request_name,
                'location' => $location_name,
                'requester'=>$this->input->post('user_type_hid'),
                'priority'=>$priority,
                'due_date'=>$date,
                'sr_title'=>$this->input->post('sr_title'),
                'sr_description'=>$this->input->post('sr_description'),
                'sr_status'=>'Open',
                'assign_name'=>$assign_name,
                'requester_name'=>$this->get_assign_name($requester_id),
            );

            if($assign_id == 0)
            {
                $cond = "FIND_IN_SET ('".$this->input->post('service_request_id')."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
                $query3 = "SELECT emailid,user_id,first_name FROM vg_user_master WHERE $cond ";
                $email_send_data = $this->master_model->custom_query($query3);

                if(!empty($email_send_data))
                {
                    foreach ($email_send_data as $email_send_data_row) {
                        $viewData['mail_data']['support_name'] = $email_send_data_row->first_name;
                        $viewData['mail_data']['email_text_support'] = 'New service request added. Please find below SR details.';
                        $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                        $this->send_email($email_send_data_row->emailid,'New Service Request',$msg_user);
                    }
                }
            }

            if($user_type == 'support')
            {
                $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
                $viewData['mail_data']['email_text_support'] = 'Your service request is created successfully. Please find your service request details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->user_id),'New Service Request',$msg_user);

                if($sr_data->assign_id != 0)
                {
                    $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                    $viewData['mail_data']['email_text_support'] = 'New service request added. Please find below SR details.';
                    $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                    $this->send_email($this->get_email_id($sr_data->assign_id),'New Service Request',$msg_user);
                }
            }
            
            if($user_type == 'employee')
            {
                if($sr_data->assign_id != 0)
                {
                    $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                    $viewData['mail_data']['email_text_support'] = 'New service request added. Please find below SR details.';
                    $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                    $this->send_email($this->get_email_id($sr_data->assign_id),'New Service Request',$msg_user);
                }

                $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
                $viewData['mail_data']['email_text_support'] = 'Your service request is created successfully. Please find your service request details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->user_id),'New Service Request',$msg_user);
            }

            $this->session->set_flashdata('success_msg','Service request added successfully.');
            redirect('service-request');
        }
        $this->get_footer();
    }


    public function getsrrequest()
    {
    	$user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
    	$sKeyword = trim($this->input->post('sKeyword'), " ");
       	$location_id = $this->input->post('location_id');
    	$request_date = $this->input->post('request_date');
        $priority = $this->input->post('priority');
    	$sr_status = $this->input->post('sr_status');
        
        $query = '';

        if(!empty($user_id) && $user_type == 'employee') 
        {
            $query .= " AND user_id = $user_id ";
        }
        
        $assign_id = '';
        $title_search = '';
        if(!empty($sKeyword))
        {   
            $query_1 = "SELECT  GROUP_CONCAT(user_id) as assign_id FROM vg_user_master WHERE  concat(first_name , ' ' , last_name) LIKE  '".$sKeyword."' ";
           //$query_1 = "SELECT  GROUP_CONCAT(user_id) as assign_id FROM vg_user_master WHERE user_type = 'support' AND first_name LIKE '".$sKeyword."%' OR last_name LIKE  '".$sKeyword."%' ";
            $user_ids =$this->master_model->custom_query($query_1);

            if(!empty($user_ids) && $user_ids[0]->assign_id !='')
            {
                $assign_id = $user_ids[0]->assign_id;
                $query .= " AND assign_id IN ($assign_id)";
                //$title_search = " OR TRIM(sr_title) LIKE  '%".$sKeyword."%' ";
                //$query .= " AND (assign_id IN ($assign_id) $title_search) ";
            }
            else
            {
                $query .= " AND TRIM(sr_title)  LIKE  '%".$sKeyword."%'  ";
            }
        }

        if(!empty($location_id))
        {
            $query .= " AND location_id = $location_id ";
        }


        if(!empty($request_date))
        {
        	$date =  $request_date;
            $date = str_replace('-', '/', $date);
           	$request_date = date('Y-m-d', strtotime($date));

            $query .= " AND DATE(dCreatedDate) = '$request_date' ";
        } 

        if(!empty($priority))
        {
            if(strpos($priority, 'All') === false)
            {
            	$query .= ' AND priority IN ('.$priority.') ';
            }
        }

        if(!empty($sr_status))
        {
        	if(strpos($priority, 'All') === false)
            {
            	//$sr_status = "'".implode ( "','", $sr_status)."'";
            	$query .= ' AND sr_status IN ('.$sr_status.') ';
            }
        }

        if (!empty($user_id) && $user_type == 'support') 
        {

            $cond=array(
                'user_id'=>$user_id,
                'user_status' =>1,
                'isDelete'=>0,
            );
            $filed=array('category_id','service_request_id');
            $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);

            if(!empty($user_data_master))
            {
                $category_id = $user_data_master[0]->category_id;
                $service_request_id = $user_data_master[0]->service_request_id;
                //$query .= " AND ((category_id In ($category_id) AND service_request_id In ($service_request_id) AND assign_id  = 0) OR (assign_id  != 0 AND  assign_id = $user_id)) ";
                $query .= " AND (category_id In ($category_id) AND service_request_id In ($service_request_id)) ";
            }
        }

      
    	$cond = "isDelete = 0 $query Order By sr_id DESC";   
        $query = "SELECT count(user_id) as total_count FROM vg_sr_master WHERE $cond ";
        $totalData =$this->master_model->custom_query($query);
        
        $total_row = $totalData[0]->total_count;

        // add pagination limit
        $limit = $_POST['length'];
        $start = $_POST['start'];
        $cond .= " LIMIT $limit  OFFSET $start";

        
        $query1 = "SELECT * FROM vg_sr_master WHERE $cond ";
        $sr_data = $this->master_model->custom_query($query1);


    	$data = array();
        if(!empty($sr_data))
        {
            foreach ($sr_data as $sr_data_row) 
            {
                $location_name = $this->get_location($sr_data_row->location_id);

                $class= "";
                if($sr_data_row->priority == 'High')
                {
                    $class= "high-priority";
                }
                elseif ($sr_data_row->priority == 'Low') {
                    $class= "low-priority";
                }
                elseif ($sr_data_row->priority == 'Medium') {
                     $class= "medium-priority";
                }

                $status_class_text= "";
                if($sr_data_row->sr_status == 'Deleted')
                {
                    $status_class_text= "red-color";
                }
                elseif ($sr_data_row->sr_status == 'Closed') {
                    $status_class_text= "pink-color";
                }
                elseif ($sr_data_row->sr_status == 'Open') {
                    $status_class_text= "green-color";
                }
                elseif ($sr_data_row->sr_status == 'InProgress') {
                    $status_class_text= "blue-color";
                }


                $assign_name =  $this->get_assign_name($sr_data_row->assign_id);
            	
                if(empty($assign_name))
                {
                    $assign_name = 'Not Assign';
                }

                $status_class = '';
                if($sr_data_row->sr_status == 'Closed')
                {
                   $status_class = ' closed-request'; 
                }

               	$due_date = '-';
                if(!empty($sr_data_row->due_date))
                {
                	$due_date = date("m-d-Y",strtotime($sr_data_row->due_date));
                }

                $priority = "-";
                if(!empty($sr_data_row->priority))
                {
                    $priority = $sr_data_row->priority;
                }

                $nestedData = array();
                
                $html_data = '<div class="request-individual'.$status_class.'">
                                    
                                    <div class="requset-to-wrap grid_request">
                                        
                                        <div class="request-to">
                                        <div class="request-title"><a href="'.base_url('service-request'.'/'.$sr_data_row->sr_id).'">'.$sr_data_row->sr_title.' - '.'<span>#VG'.$sr_data_row->sr_id.'</span></a></div>
                                            <div class="grid_inner">
                                                <div class="request-item assign">
                                                <div class="small-title">assigned to</div>
                                                <span class="small-title-detail">'.$assign_name.'</span>
                                            </div>
                                            <div class="request-item due-date">
                                                <div class="small-title">Need by date</div>
                                                <span class="small-title-detail">'.$due_date.'</span>
                                            </div>
                                            <div class="request-item stuts">
                                                <div class="small-title">status</div>
                                                <span class="small-title-detail '.$status_class_text.'">'.$sr_data_row->sr_status.'</span>
                                            </div>
                                            <div class="request-item location">
                                                <div class="small-title">location</div>
                                                <span class="small-title-detail">'.$location_name.'</span>
                                            </div>';
                                            
                                            // if($user_type == 'support') 
                                            // {
                                            $html_data .= '<div class="request-item priority">
                                                <div class="small-title">priority</div>
                                                <span class="small-title-detail '.$class.'">'.$priority.'</span>
                                            </div>';
                                            // }
                        $html_data .= '</div>
                                        </div>
                                        <div class="request-actions">
                                            <div class="action-buttons">
                                                <a href="" class="reopen-button" data-toggle="modal" data-target="#reopenRequestmodal" data-id="'.$sr_data_row->sr_id.'"><i class="venita-reopen-icon"></i>Reopen</a>
                                                <div class="view-details-button">
                                                    <a href="'.base_url('service-request'.'/'.$sr_data_row->sr_id).'" class="btn pink-outline-btn">view details</a>
                                                </div>
                                            </div>
                                            <div class="requested-on-date">requested on: <span>'.date("m-d-Y",strtotime($sr_data_row->dCreatedDate)).'
                                            </span></div>
                                        </div>
                                    </div>
                                </div>';
                $nestedData[] = $html_data;
         		$data[] = $nestedData;  
            }
        }

        $json_data = array(
                'draw'            => $_POST['draw'],
                'recordsTotal'    => $total_row,
                'recordsFiltered' => $total_row,
                'data'            => $data
        );
        echo json_encode($json_data);

    }

    public function support_list($sr_id)
    {
        $cond=array(
            'sr_id'=>$sr_id,
            'isActive' =>1,
            'isDelete'=>0,
        ); 
        $filed=array('category_id','service_request_id');
        $sr_master=$this->master_model->get_record_fields('vg_sr_master',$filed,$cond);
        if(!empty($sr_master))
        {
            $cond= "FIND_IN_SET ('".$sr_master[0]->category_id."',category_id) AND FIND_IN_SET ('".$sr_master[0]->service_request_id."',service_request_id) AND user_status = 1  AND isDelete = 0";
            $query1 = "SELECT user_id,first_name,last_name FROM vg_user_master WHERE $cond";
            $assign_data = $this->master_model->custom_query($query1);
            return $assign_data;
        }
    }

    public function set_assign_id()
    {
        $dModifyDate = date("Y-m-d H:i:s");
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $assign_id = $this->input->post('assign_id');
        $sr_id =  $this->input->post('sr_id');
        $sr_status =  $this->input->post('sr_status');

        $data=array(
            'assign_id'=> $assign_id,
            'dModifyDate' =>$dModifyDate,
        );

        $cond=array(
            'sr_id'=>$sr_id,
        );
        $this->master_model->update_record('vg_sr_master',$data,$cond);

        $employee_read_status = 0;
        $support_read_status = 0;
        if($user_type == 'support')
        {
            $employee_read_status = 1;
        }
        elseif($user_type == 'employee')
        {
            $support_read_status = 1;
        }

        $data=array(
            'sr_id'=>$sr_id,
            'user_id'=>$user_id,
            'user_type'=>$user_type,
            'assign_id'=>$assign_id,
            'sr_status' =>$sr_status,
            'activity_by'=>$assign_id,
            'activity_type'=>'Assign To',
            'employee_read_status'=>$employee_read_status,
            'support_read_status'=>$support_read_status,
            'dCreatedDate' => $dModifyDate,
        );
        $this->master_model->insert_record('vg_request_history',$data);

        //Email Send
        $sr_data = $this->get_sr_details($sr_id);

        if(!empty($sr_data) && !empty($sr_data->due_date))
        {
            $date = date('m-d-Y', strtotime($sr_data->due_date));
        }

        $assign_name = '';
        if(!empty($sr_data) && $sr_data->assign_id != 0)
        {
            $assign_name = $this->get_assign_name($sr_data->assign_id);
        }
        $category_name =  $this->get_category($sr_data->category_id);
        $service_request_name =  $this->get_srtype($sr_data->service_request_id);
        $location_name =   $this->get_location($sr_data->location_id);

        $viewData['mail_data']=array(
            'email_title'=>'Update Service Request',
            'email_text'=>'Your service request assigned to our support member. Please find your service request details below.',
            'name'=>$sr_data->name,
            'emailid'=>$sr_data->emailid,
            'category_name' =>$category_name,
            'service_request_name' =>$service_request_name,
            'location' => $location_name,
            'requester'=>ucfirst($sr_data->requester_type),
            'priority'=>$sr_data->priority,
            'due_date'=>$date,
            'sr_title'=>$sr_data->sr_title,
            'sr_description'=>$sr_data->sr_description,
            'sr_status'=>$sr_status,
            'assign_name'=>$assign_name,
            'requester_name'=>$this->get_assign_name($sr_data->requester_id),
        );

        $msg_user = $this->load->view('email-templates/service-request', $viewData, true);

        //Email Send Employee  Email
        $this->send_email($this->get_email_id($sr_data->user_id),'Service Request Assigned',$msg_user);
        
        //Email Send Support.
        $viewData['mail_data']['support_name'] =  $assign_name;
        $viewData['mail_data']['email_text_support'] = 'Service request assigned to you. Please find SR details below.';
        $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
        $this->send_email($this->get_email_id($sr_data->assign_id),'Service Request Assigned',$msg_user);
                
        $this->session->set_flashdata('success_msg','Service request assigned successfully.');
        redirect('service-request');

        // $assign_data = $this->support_list($sr_id);
        // $data = '';
        // $data .= '<option value="">Select Assign To</option>';
        // foreach ($assign_data as $assign_data_row) { 
            
        //     $select = '';
        //     if($assign_data_row->user_id == $assign_id)
        //     {
        //         $select = 'selected';
        //     }

        //     $data .= '<option value="'.$assign_data_row->user_id.'" '.$select.' >'.$assign_data_row->first_name.' '.$assign_data_row->last_name.'</option>';
        //  }
        // echo json_encode(array("result"=>"success","data"=>$data));

    }
    
    public function get_location($location_id)
    {
        $cond=array(
            'location_id'=>$location_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $filed=array('location_name');
        $location_master=$this->master_model->get_record_fields('vg_location_master',$filed,$cond);
        return $location_master[0]->location_name;
    }

    public function get_assign_name($user_id)
    {
        $cond=array(
            'user_id'=>$user_id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('first_name','last_name');
        $user_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        if(!empty($user_master))
        {
            return $user_master[0]->first_name.' '.$user_master[0]->last_name;
        }
    }

    public function get_category($category_id)
    {
        $cond=array(
            'category_id'=>$category_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $filed=array('category_name');
        $category_master=$this->master_model->get_record_fields('vg_category',$filed,$cond);
        return $category_master[0]->category_name;
    }

    public function get_srtype($service_request_id)
    {
        $cond=array(
            'service_request_id'=>$service_request_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $filed=array('service_request_name');
        $service_request_master=$this->master_model->get_record_fields('vg_service_request',$filed,$cond);
        return $service_request_master[0]->service_request_name;
    }


    public function srdetails($sr_id)
    {
        $this->config->config["page_title"]= 'SR Details';
    	$user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        if(!empty($user_id))
        {
            if(!empty($user_type) && $user_type == 'support')
            {
                $cond=array(
                    'user_id'=>$user_id,
                    'user_status' =>1,
                    'isDelete'=>0,
                );
                $filed=array('category_id','service_request_id');
                $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);

                if(!empty($user_data_master))
                {
                    $category_id = $user_data_master[0]->category_id;
                    $service_request_id = $user_data_master[0]->service_request_id;
                    //$cond = " (category_id In ($category_id) AND service_request_id In ($service_request_id) AND assign_id  = 0) OR (assign_id  != 0 AND  assign_id = $user_id)  AND isActive = 1 and isDelete = 0";
                    $cond = " (category_id In ($category_id) AND service_request_id In ($service_request_id))  AND isActive = 1 and isDelete = 0";
                }
            }
            elseif(!empty($user_type) && $user_type == 'employee')
            {
                $cond = "user_id = $user_id and isActive = 1 and isDelete = 0";
            }

            $query = "SELECT sr_id FROM vg_sr_master WHERE $cond ";
            $get_sr_ids = $this->master_model->custom_query($query);

            if(!empty($get_sr_ids))
            {   
                foreach ($get_sr_ids as $get_sr_id_row) {
                    $sr_ids[] = $get_sr_id_row->sr_id;
                }
                
                if (!in_array($sr_id,$sr_ids)) 
                {
                    $this->session->set_flashdata('error_msg',"You don't have permission to view this SR.");
                    redirect('service-request'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error_msg',"You don't have permission to view this SR.");
                redirect('service-request'); 
            }
        }
        
    	$this->get_header();
    	$data = array();

    	$cond=array(
            'user_id'=>$user_id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('category_id');
        $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        $q= '';
        if(!empty($user_data_master) && !empty($user_data_master[0]->category_id) && $user_type == 'support')
        {
            $category_id  = $user_data_master[0]->category_id;
            $q  = " AND category_id IN ($category_id) ";
        }
        $query = "SELECT * FROM vg_category WHERE isActive = 1 AND isDelete = 0 $q ";
        $data['category_data'] = $this->master_model->custom_query($query);

        $cond=array('isActive' =>1,'isDelete'=>0);
    	$data['location_data']=$this->master_model->get_record('vg_location_master',$cond);

    	$cond=array('sr_id'=>$sr_id,'isActive' =>1);
    	$data['sr_data']=$this->master_model->get_one_record('vg_sr_master',$cond);

    	$cond=array('sr_id'=>$sr_id,'isActive' =>1,'isDelete'=>0);
    	$data['sr_attachment']=$this->master_model->get_record('vg_sr_attachment',$cond);

        $cond= "sr_id= $sr_id AND isActive = 1 AND isDelete = 0 Order by history_id DESC";
        $query1 = "SELECT * FROM vg_request_history WHERE $cond";
        $data['request_history'] = $this->master_model->custom_query($query1);

        $cond = '';

        
        // if($user_type == 'support')
        // {
        //     $cond .= "((sender_id = $user_id and sender_role= 'support' ) or ( receiver_id = $user_id and receiver_role = 'support')) AND "; 
        // }
        // else

        // if($user_type == 'employee')
        // {  
        //     $cond .= "((sender_id = $user_id and sender_role= 'employee' ) or ( receiver_id = $user_id and receiver_role = 'employee')) AND ";
        // }

        $cond .= " sr_id= $sr_id AND isActive = 1 AND isDelete = 0 Order by dCreatedDate ASC";
        $query1 = "SELECT * FROM vg_messages WHERE $cond";
        $data['message_data'] = $this->master_model->custom_query($query1);


        // $cond  = array('vg_messages.sr_id'=>$sr_id);
        // $table1 = 'vg_messages';
        // $table2 = 'vg_reply_message';
        // $common1 = 'message_id';
        // $common2 = 'message_id';
        // $message_data =$this->master_model->get_record_with_join($table1,$table2,$common1,$common2,$cond);        
        // var_dump($message_data);

    	$this->load->view('service-request/service-details',$data);
    	$this->get_footer();
    }

    public function updatesr()
    {
        $dModifyDate = date("Y-m-d H:i:s");
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        
        $date = '';
        $due_date = Null;
        if(!empty($this->input->post('due_date')))
        {
            $date =  $this->input->post('due_date');
            $date = str_replace('-', '/', $date);
            $due_date = date('Y-m-d', strtotime($date));
        }

        $priority = '';
        if(!empty($this->input->post('priority')))
        {
        	$priority = $this->input->post('priority');
        }

        // echo $priority;
        // exit();

        $sr_status = $this->input->post('sr_status');
        if(empty($sr_status))
        {
            $sr_status = $this->input->post('sr_status_hid');
        }

        $data=array(
            'category_id'=>$this->input->post('category_id_hid'),
            'service_request_id' =>$this->input->post('service_request_id'),
            'assign_id'=>$this->input->post('assign_id_edit'),
            'location_id'=>$this->input->post('location_id'),
            'priority'=>$priority,
            'due_date'=>$due_date,
            'sr_title'=>$this->input->post('sr_title'),
            'sr_description'=>$this->input->post('sr_description'),
            'sr_status'=>$sr_status,
            'dModifyDate' =>$dModifyDate,
        );
        $cond=array(
           	'sr_id'=>$this->input->post('sr_id'),
        );
        $this->master_model->update_record('vg_sr_master',$data,$cond);


        if (!empty($_FILES['attachment_file']['name']))
        {
           foreach ($_FILES['attachment_file']["name"] as $file=>$key) 
           {
                if (!empty($_FILES['attachment_file']["name"][$file])) 
                {
                    $file_name = $_FILES['attachment_file']["name"][$file];
                    $cond = array("sattachment"=>$file_name,'isDelete' => 0);
                    $rec = $this->master_model->get_record('vg_sr_attachment',$cond);
                    $results = count($rec);

                    if($results > 0)
                    {
                        $file_name = date('m-d-Y-h-i-s').'-'.$file_name;
                    }

                    $_FILES['nsImage'] ["name"] = $file_name;
                    $_FILES['nsImage'] ["type"] = $_FILES['attachment_file']["type"][$file];
                    $_FILES['nsImage'] ["tmp_name"] = $_FILES['attachment_file']["tmp_name"][$file];
                    $_FILES['nsImage'] ["error"] = $_FILES['attachment_file']["error"][$file];
                    $_FILES['nsImage'] ["size"] = $_FILES['attachment_file']["size"][$file];


                    $photo=$this->do_upload('nsImage','sr_documents');
                    // var_dump($photo);
                    // exit();
                    $data=array(
                        'user_id'=>$user_id,
                        'sr_id'=>$this->input->post('sr_id'),
                        'sattachment' =>$photo['upload_data']['file_name'],
                        'dCreatedDate' => $dModifyDate,
                    );
                    $attachment_id = $this->master_model->insert_record('vg_sr_attachment',$data);
                }
            }
        }

        
        //Check Category And SR Update Time Assign User
        $assign_id_edit_check = $this->input->post('assign_id_edit');
        $category_id_check = $this->input->post('category_id_hid');
        $service_request_id_check = $this->input->post('service_request_id');

        if($user_type == 'employee' && !empty($assign_id_edit_check))
        {
            $check_valid_cat_sr = $this->checkcatsrtype($assign_id_edit_check,$category_id_check,$service_request_id_check);
            if(empty($check_valid_cat_sr))
            {
                $data=array(
                    'assign_id'=>0,
                );
                $cond=array(
                    'sr_id'=>$this->input->post('sr_id'),
                );
                $this->master_model->update_record('vg_sr_master',$data,$cond);

            }
        } 

        $sr_data = $this->get_sr_details($this->input->post('sr_id'));

        $assign_id = 0;
        $assign_name = '';
        if(!empty($sr_data) && $sr_data->assign_id != 0)
        {
            $assign_id = $sr_data->assign_id;
            $assign_name = $this->get_assign_name($sr_data->assign_id);
        }

        $employee_read_status = 0;
        $support_read_status = 0;
        if($user_type == 'support')
        {
            $employee_read_status = 1;
        }
        elseif($user_type == 'employee')
        {
            $support_read_status = 1;
        }

        //$activity_type = 'SR Request updated By';
        // Update History
        if(!empty($sr_data))
        {
            $data=array(
                'sr_id'=>$this->input->post('sr_id'),
                'user_id'=>$user_id,
                'user_type'=>$user_type,
                'assign_id'=>$assign_id,
                'sr_status' =>$sr_status,
                'activity_by'=>$user_id,
                'employee_read_status'=>$employee_read_status,
                'support_read_status'=>$support_read_status,
                'dCreatedDate' => $dModifyDate,
            );
            if($sr_data->category_id != $this->input->post('category_id_hid'))
            {
                $activity_type = 'SR Category updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->service_request_id != $this->input->post('service_request_id'))
            {
                $activity_type = 'SR Type updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->assign_id != $this->input->post('assign_id_edit') && $this->input->post('assign_id_edit') != 0)
            {
                $activity_type = 'SR Assigned To updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->assign_id != 0 && $this->input->post('assign_id_edit') == 0)
            {
                $activity_type = 'SR Assigned To Remove By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->location_id != $this->input->post('location_id'))
            {
                $activity_type = 'SR Location updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if(!empty($priority) && $sr_data->priority != $priority)
            {
                $activity_type = 'SR Priority updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->due_date != $due_date)
            {
                $activity_type = 'SR Need by Date updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->sr_title != $this->input->post('sr_title'))
            {
                $activity_type = 'SR Title updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if($sr_data->sr_description != $this->input->post('sr_description'))
            {
                $activity_type = 'SR Description updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
            if(!empty($attachment_id))
            {
                $activity_type = 'SR Attachment updated By';
                $data['activity_type'] = $activity_type;
                $this->master_model->insert_record('vg_request_history',$data);
            }
        }

        $category_name =  $this->get_category($sr_data->category_id);
        $service_request_name =  $this->get_srtype($this->input->post('service_request_id'));
        $location_name =   $this->get_location($this->input->post('location_id'));

        $viewData['mail_data']=array(
            'email_title'=>'Service Request Updated',
            'email_text'=>'Your service request updated successfully. Please find your service request details below.',
            'name'=>$sr_data->name,
            'emailid'=>$sr_data->emailid,
            'category_name' =>$category_name,
            'service_request_name' =>$service_request_name,
            'location' => $location_name,
            'requester'=>ucfirst($sr_data->requester_type),
            'priority'=>$priority,
            'due_date'=>$date,
            'sr_title'=>$this->input->post('sr_title'),
            'sr_description'=>$this->input->post('sr_description'),
            'sr_status'=>$sr_status,
            'assign_name'=>$assign_name,
            'requester_name'=>$this->get_assign_name($sr_data->requester_id),
        );

        //$msg_user = $this->load->view('email-templates/service-request', $viewData, true);

        //Email Send
        //$this->send_email($sr_data->emailid,'Update Service Request',$msg_user);

        if($sr_data->assign_id == 0)
        {
            $cond = "FIND_IN_SET ('".$this->input->post('service_request_id')."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
            $query3 = "SELECT emailid,user_id,first_name FROM vg_user_master WHERE $cond ";
            $email_send_data = $this->master_model->custom_query($query3);

            if(!empty($email_send_data))
            {
                foreach ($email_send_data as $email_send_data_row) {
                    $viewData['mail_data']['support_name'] = $email_send_data_row->first_name;
                    $viewData['mail_data']['email_text_support'] = 'Service request update. Please find SR details below.';
                    $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                    $this->send_email($email_send_data_row->emailid,'Service Request Updated',$msg_user);
                }
            }
        }

        if($user_type == 'support')
        {
            $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
            $viewData['mail_data']['email_text_support'] = 'Service request update. Please find SR details below.';
            $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
            $this->send_email($this->get_email_id($sr_data->user_id),'Service Request Updated',$msg_user);

            if($sr_data->assign_id != 0)
            {
                 $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                $viewData['mail_data']['email_text_support'] = 'Service request update. Please find SR details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->assign_id),'Service Request Updated',$msg_user);
            }
        }
        
        if($user_type == 'employee')
        {
            if($sr_data->assign_id != 0)
            {
                $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                $viewData['mail_data']['email_text_support'] = 'Service request update. Please find SR details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->assign_id),'Service Request Updated',$msg_user);
            }

            $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
            $viewData['mail_data']['email_text_support'] = 'Service request update. Please find SR details below.';
            $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
            $this->send_email($this->get_email_id($sr_data->user_id),'Service Request Updated',$msg_user);   
        }

        $this->session->set_flashdata('success','Service request updated successfully.');
        redirect($this->agent->referrer()); 
    }

    public function checkcatsrtype($user_id,$category_id,$service_request_id)
    {
        $query = "SELECT user_id FROM vg_user_master WHERE (FIND_IN_SET ('$category_id',category_id) AND FIND_IN_SET ('$service_request_id',service_request_id)) AND user_status = 1  AND isDelete = 0";
        $get_record = $this->master_model->custom_query($query);
        if(!empty($get_record))
        {
            return $get_record[0]->user_id;
        }
    }

    public function get_reply_msg($message_id)
    {
        // $cond=array('message_id'=>$message_id,'isActive' =>1,'isDelete'=>0);
        // $get_reply_data =$this->master_model->get_record('vg_reply_message',$cond);
        // return $get_reply_data;
        $cond=array(
            'message_id'=>$message_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $filed=array('message');
        $reply_data=$this->master_model->get_record_fields('vg_messages',$filed,$cond);
        if(!empty($reply_data))
        {
            return $reply_data[0]->message;
        }
    }

    public function get_email_id($user_id)
    {
        $cond=array(
            'user_id'=>$user_id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('emailid');
        $email_data=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        if(!empty($email_data))
        {
            return $email_data[0]->emailid;
        }
    }

    public function get_email_id_ajax()
    {
        $user_id = $this->input->post('user_id');
        $cond=array(
            'user_id'=>$user_id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('emailid','location_id');
        $email_data=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        if(!empty($email_data))
        {
            $emailid =  $email_data[0]->emailid;
            $location_id =  $email_data[0]->location_id;
            $location_name =  $this->get_location($email_data[0]->location_id);
            
            $data = array(
                'emailid' => $emailid,
                'location_name' =>$location_name,
                'location_id' => $location_id,
            );
            echo json_encode(array("result"=>"success","data"=>$data));
            exit();
        }
    }


    public function get_msg_attachment($message_id)
    {
        // $cond=array('message_id'=>$message_id,'isActive' =>1,'isDelete'=>0);
        // $get_reply_data =$this->master_model->get_record('vg_reply_message',$cond);
        // return $get_reply_data;
        $cond=array(
            'message_id'=>$message_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $attchment_data=$this->master_model->get_record('vg_messages_attachment',$cond);
        if(!empty($attchment_data))
        {
            return $attchment_data;
        }
    }

    public function msg_send()
    {
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        $dCreatedDate = date("Y-m-d H:i:s");

        
        $cond=array('sr_id'=>$this->input->post('sr_id'));
        $sr_data =$this->master_model->get_one_record('vg_sr_master',$cond);

        if(!empty($user_id))
        {
            $sender_id = $user_id;
        }

        $message_id = $this->input->post('message_id');
        if(!empty($message_id))
        {
            $cond=array('message_id'=>$message_id);
            $messages_data =$this->master_model->get_one_record('vg_messages',$cond);

            if(!empty($messages_data))
            {
                if($messages_data->sender_id != $sender_id)
                {
                    $receiver_id = $messages_data->sender_id;
                    $receiver_role = $messages_data->sender_role;
                }
            }
        }
        else
        {
            $message_id = 0;
        }

        // if(empty($this->input->post('message_id')))
        // {
            $data=array(
                'sr_id'=>$this->input->post('sr_id'),
                'parent_message_id'=>$message_id,
                'message'=>$this->input->post('message'),
                'sender_id'=>$sender_id,
                'sender_role'=>$this->get_role($sender_id),
                'receiver_id' =>'',
                'receiver_role'=>'',
                'dCreatedDate' => $dCreatedDate,
            );
            $message_id =$this->master_model->insert_record('vg_messages',$data);
        // }
        // elseif (!empty($this->input->post('message_id')))
        // {
        //      $data=array(
        //         'message_id'=>$this->input->post('message_id'),
        //         'sr_id'=>$this->input->post('sr_id'),
        //         'reply_message'=>$this->input->post('message'),
        //         'sender_id'=>$sender_id,
        //         'sender_role'=>$this->get_role($sender_id),
        //         'receiver_id' =>'',
        //         'receiver_role'=>'',
        //         'dCreatedDate' => $dCreatedDate,
        //     );
        //     $message_id =$this->master_model->insert_record('vg_reply_message',$data);
        // }

        
        if(!empty($message_id))
        {
            if (!empty($_FILES['attachment_file']['name']))
            {

               foreach ($_FILES['attachment_file']["name"] as $file=>$key) 
               {
                    if (!empty($_FILES['attachment_file']["name"][$file])) 
                    {
                        $file_name = $_FILES['attachment_file']["name"][$file];
                        $cond = array("sattachment"=>$file_name,'isDelete' => 0);
                        $rec = $this->master_model->get_record('vg_messages_attachment',$cond);
                        $results = count($rec);

                        if($results > 0)
                        {
                            $file_name = date('m-d-Y-h-i-s').'-'.$file_name;
                        }

                        $_FILES['nsImage'] ["name"] = $file_name;
                        $_FILES['nsImage'] ["type"] = $_FILES['attachment_file']["type"][$file];
                        $_FILES['nsImage'] ["tmp_name"] = $_FILES['attachment_file']["tmp_name"][$file];
                        $_FILES['nsImage'] ["error"] = $_FILES['attachment_file']["error"][$file];
                        $_FILES['nsImage'] ["size"] = $_FILES['attachment_file']["size"][$file];

                        $photo=$this->do_upload('nsImage','sr_message_doc');
                        //var_dump($photo);
                        $data=array(
                            'message_id'=>$message_id,
                            'sattachment' =>$photo['upload_data']['file_name'],
                            'dCreatedDate' => $dCreatedDate,
                            );
                        $this->master_model->insert_record('vg_messages_attachment',$data);
                    }
                }
            }
        }

        $assign_id = 0;
        if(!empty($sr_data) && $sr_data->assign_id != 0)
        {
            $assign_id = $sr_data->assign_id;
        }

        $employee_read_status = 0;
        $support_read_status = 0;
        if($user_type == 'support')
        {
            $employee_read_status = 1;
        }
        elseif($user_type == 'employee')
        {
            $support_read_status = 1;
        }

        $data=array(
            'sr_id'=>$this->input->post('sr_id'),
            'user_id'=>$user_id,
            'user_type'=>$user_type,
            'assign_id'=>$assign_id,
            'sr_status' =>$sr_data->sr_status,
            'activity_by'=>$user_id,
            'activity_type'=>'Replied By',
            'employee_read_status'=>$employee_read_status,
            'support_read_status'=>$support_read_status,
            'dCreatedDate' => $dCreatedDate,
        );
        $this->master_model->insert_record('vg_request_history',$data);

        $this->session->set_flashdata('success','Reply send successfully.');
        redirect($this->agent->referrer()); 

    }


    public function get_role($id)
    {
        $cond=array(
            'user_id'=>$id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('user_type');
        $user_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond);
        if(!empty($user_master))
        {
            return $user_master[0]->user_type;
        }
    }


    public function updatesrstatus()
    {
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        $dCreatedDate = date("Y-m-d H:i:s");

        $sr_id = $this->input->post('sr_id');

        $sr_status = '';
        $sr_status = $this->input->post('sr_status');
        if(empty($sr_status))
        {
            $sr_status = $this->input->post('sr_status_hid_pop');
        }

         $data=array(
            'sr_status'=>$sr_status,
            'dModifyDate' =>$dCreatedDate,
        );

        $cond=array(
            'sr_id'=>$sr_id,
        );
        $this->master_model->update_record('vg_sr_master',$data,$cond);

        if(!empty($this->input->post('sr_status_reopen')))
        {
            $sr_status = $this->input->post('sr_status_reopen');
        }

        $employee_read_status = 0;
        $support_read_status = 0;
        if($user_type == 'support')
        {
            $employee_read_status = 1;
        }
        elseif($user_type == 'employee')
        {
            $support_read_status = 1;
        }

        $cond=array('sr_id'=>$sr_id);
        $sr_data =$this->master_model->get_one_record('vg_sr_master',$cond);

        $assign_id = 0;
        $assign_name = "";
        if(!empty($sr_data) && $sr_data->assign_id != 0)
        {
            $assign_id = $sr_data->assign_id;
            $assign_name =$this->get_assign_name($sr_data->assign_id);
        }

        if(!empty($this->input->post('reason')) && !empty($this->input->post('comment')))
        {
            $activity_type =  "SR $sr_status By";
            $history_data=array(
                'sr_id'=>$sr_id,
                'user_id'=>$user_id,
                'user_type'=>$user_type,
                'assign_id'=>$assign_id,
                'sr_status' =>$sr_status,
                'activity_by'=>$user_id,
                'activity_type'=>$activity_type,
                'reason'=> $this->input->post('reason'),
                'comment'=>$this->input->post('comment'),
                'employee_read_status'=>$employee_read_status,
                'support_read_status'=>$support_read_status,
                'dCreatedDate' => $dCreatedDate,
            );
        }
        else
        {
            $history_data=array(
                'sr_id'=>$sr_id,
                'user_id'=>$user_id,
                'user_type'=>$user_type,
                'assign_id'=>$assign_id,
                'sr_status' =>$sr_status,
                'activity_by'=>$user_id,
                'activity_type'=>'Status updated By',
                'employee_read_status'=>$employee_read_status,
                'support_read_status'=>$support_read_status,
                'dCreatedDate' => $dCreatedDate,
            );
        }

        $this->master_model->insert_record('vg_request_history',$history_data);
        
        $date = "";
        if(!empty($sr_data->due_date))
        {
            $date = date('m-d-Y',strtotime($sr_data->due_date));
        }

        $category_name =  $this->get_category($sr_data->category_id);
        $service_request_name =  $this->get_srtype($sr_data->service_request_id);
        $location_name =   $this->get_location($sr_data->location_id);

        $viewData['mail_data']=array(
            'email_title'=>'SR Status Update',
            'email_text'=>'SR status update. Please find your service request details below.',
            'name'=>$sr_data->name,
            'emailid'=>$sr_data->emailid,
            'category_name' =>$category_name,
            'service_request_name' =>$service_request_name,
            'location' => $location_name,
            'requester'=>ucfirst($sr_data->requester_type),
            'priority'=>$sr_data->priority,
            'due_date'=>$date,
            'sr_title'=>$sr_data->sr_title,
            'sr_description'=>$sr_data->sr_description,
            'sr_status'=>$sr_status,
            'assign_name'=>$assign_name,
            'requester_name'=>$this->get_assign_name($sr_data->requester_id),
        );

        if($sr_data->assign_id == 0)
        {
            $cond = "FIND_IN_SET ('".$this->input->post('service_request_id')."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
            $query3 = "SELECT emailid,user_id,first_name FROM vg_user_master WHERE $cond ";
            $email_send_data = $this->master_model->custom_query($query3);

            if(!empty($email_send_data))
            {
                foreach ($email_send_data as $email_send_data_row) {
                    $viewData['mail_data']['support_name'] = $email_send_data_row->first_name;
                    $viewData['mail_data']['email_text_support'] = 'SR status update. Please find SR details below.';
                    $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                    $this->send_email($email_send_data_row->emailid,'SR Status Update',$msg_user);
                }
            }
        }
        
        if($user_type == 'support')
        {
            $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
            $viewData['mail_data']['email_text_support'] = 'SR status update. Please find SR details below.';
            $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
            $this->send_email($this->get_email_id($sr_data->user_id),'SR Status Update',$msg_user);

            if($sr_data->assign_id != 0)
            {
                $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                $viewData['mail_data']['email_text_support'] = 'SR status update. Please find SR details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->assign_id),'SR Status Update',$msg_user);
            }
        }
        
        if($user_type == 'employee')
        {
            if($sr_data->assign_id != 0)
            {
                $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->assign_id);
                $viewData['mail_data']['email_text_support'] = 'SR status update. Please find SR details below.';
                $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
                $this->send_email($this->get_email_id($sr_data->assign_id),'SR Status Update',$msg_user);
            }

            $viewData['mail_data']['support_name'] = $this->get_assign_name($sr_data->user_id);
            $viewData['mail_data']['email_text_support'] = 'SR status update. Please find SR details below.';
            $msg_user = $this->load->view('email-templates/service-request', $viewData, true);
            $this->send_email($this->get_email_id($sr_data->user_id),'SR Status Update',$msg_user);

        }
        
        if($this->input->post('from_ajax') == 'Yes')
        {
            echo json_encode(array("result"=>"success","status_change"=>"true"));
            exit();
        }
        else
        {
            $this->session->set_flashdata('success',"Request $sr_status successfully.");
        }
        redirect($this->agent->referrer()); 
    }
}
?>
