<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'Front_master.php';

class Front extends Front_master
{
	public function __construct()
	{
		parent:: __construct();
		if($this->session->userdata('user_type') == 'admin')
		{
			redirect('admin');
		}
		elseif ($this->session->userdata('user_type') == 'employee' || $this->session->userdata('user_type') == 'support') {
			redirect('service-request');
		}
    }
    
	public function index()
	{
		$this->config->config["page_title"]= 'Login';
		$this->get_header();
		if($this->input->post('page_for') == 'login')
		{
			$this->form_validation->set_rules('user_name', 'Please enter user name.', 'required');
			$this->form_validation->set_rules('password', 'Please enter password.', 'required');
		}
		
		
		$redirect =  base_url(); 		
		if($this->form_validation->run() == FALSE)
		{	
			$remember_me = get_cookie('remember_me');
			$data = array();
				
			if($remember_me)
			{
				$user_id = get_cookie('user_id');
				$cond=array('user_id'=>$user_id,'user_status' => 1);
				$data['user_data']= $this->master_model->get_one_record("vg_user_master",$cond);
			}
			
			$this->load->view('front/login',$data);
		}
		else
		{
			if($this->input->post('page_for') == 'login')
			{
				// $cond=array(
				// 'user_name'=>$this->input->post('user_name'),
				// 'password'=>md5($this->input->post('password')),
				// //'user_status'=>1,
				// 'isDelete' =>0,
				// );
				// $result = $this->master_model->validate_user('vg_user_master',$cond);
				$user_name = $this->input->post('user_name');
				$password = md5($this->input->post('password'));
				$cond= " (user_name COLLATE latin1_general_cs = '$user_name' or emailid COLLATE latin1_general_cs = '$user_name') AND password = '$password' ";
	            $query3 = "SELECT * FROM vg_user_master WHERE $cond ";
	            $result = $this->master_model->custom_query($query3);
				$result = $result[0];

				if($result)
				{
					if($result->user_status == 2)
					{
						$this->session->set_flashdata('error_msg', 'Your account deactivated by administer.');
						redirect(base_url());
					}
					elseif($result->user_status == 3)
					{
						$this->session->set_flashdata('error_msg', 'Your account deleted by administer. If any query please contact admin.');
						redirect(base_url());
					}

					$remember_me   = $this->input->post('remember_me');

					if($remember_me == 1)
					{
						$password = safe_b64encode($this->input->post('password'));
						$this->input->set_cookie('remember_me',true, time()+2678400,false);
						$this->input->set_cookie('password',$password, time()+2678400,false);
					}
					else
					{
						delete_cookie("remember_me");
						delete_cookie("password");
					}

					$data = array(
			            'user_id' => $result->user_id,
						'first_name' => $result->first_name,
						'last_name' => $result->last_name,
						'user_type' => $result->user_type,
			       	); 

					$this->input->set_cookie('user_id', $result->user_id, time()+2678400,false);
					$this->session->set_userdata($data);
					$user_id=$this->session->userdata('user_id');

					if($result->user_type == 'employee' || $result->user_type == 'support')
					{
						redirect('service-request');
					}
					else if($result->user_type == 'admin') 
					{
						redirect('admin');
					}
				}
				else
				{
					$this->session->set_flashdata('error_msg', 'Please enter valid username / email address or password.');
					redirect(base_url());
				}
			}
		}

		$this->get_footer();
	}

	public function forgotpassword()
	{
		$forgot_user_name = $this->input->post("forgot_user_name");

		if(!empty($forgot_user_name))
		{
			$cond= " (user_name COLLATE latin1_general_cs = '$forgot_user_name' or emailid COLLATE latin1_general_cs = '$forgot_user_name') AND user_status = 1 ";
			//$cond = "FIND_IN_SET ('".$this->input->post('service_request_id')."',service_request_id) AND user_status = 1  AND isDelete = 0"; 
            $query3 = "SELECT * FROM vg_user_master WHERE $cond ";
            $user_data = $this->master_model->custom_query($query3);
			//$user_data = $this->master_model->get_one_record("vg_user_master",$cond);
			if(!empty($user_data))
			{
	        	$user_data= $user_data[0];   
				// $password =  $this->randomPassword();
				// $data=array(
				// 	'password'=>md5($password),
				// );
				// $cond=array('user_id'=>$user_data->user_id);
				// $this->master_model->update_record('vg_user_master',$data,$cond);
				$this->load->library('encryption');
				$str="$user_data->emailid&$user_data->password&$user_data->user_type";
				$encrypted_string=urlencode(base64_encode($str));
				$forgot_link=base_url('reset-password/'.$encrypted_string);

				$viewData['mail_data']=array(
					'first_name'=>$user_data->first_name,
					'forgot_link'=>$forgot_link
				);

				$msg_user = $this->load->view('email-templates/forgot_password', $viewData, true);

				//Email Send
				$this->send_email($user_data->emailid,'Forgot Password',$msg_user);	

				echo json_encode(array("result"=>"success","mail_send"=>"true"));
			}
			else
			{
				echo json_encode(array("result"=>"success","mail_send"=>"false"));
			}
			exit();
		}		
	}

	public function reset_password($encrypted_string)
	{
		$this->config->config["page_title"]='Reset Password';
		$this->get_header();
		$this->form_validation->set_rules('password', 'Please enter new password.', 'required');
		$this->form_validation->set_rules('confirm_password', 'Please enter confirm  password.', 'required');
 
	    if ($this->form_validation->run() == FALSE)
		{
			$this->load->library('encryption');
			$str=base64_decode(urldecode($encrypted_string));
			$str_arr=explode('&',$str);
			if(count($str_arr) > 1)
			{
				$email=$str_arr[0];
				$password=$str_arr[1];
				$user_type=$str_arr[2];
			}
			else
			{
				$email='';
				$password='';
				$user_type='';
			}
			

			$cond=array(
				'emailid'=>$email,
				'password'=>$password,
				'user_type'=>$user_type,
				'user_status'=>1,
				'isDelete'=>0
			);
			$user_data_get=$this->master_model->get_one_record('vg_user_master',$cond);
		
			if(!empty($user_data_get))
			{
				$data = array(
		            'reset_user_id' => $user_data_get->user_id,
					// 'first_name' => $user_data->first_name,
					// 'last_name' => $user_data->last_name,
					'reset_user_type' => $user_data_get->user_type,
		       	); 
				//$this->input->set_cookie('reset_user_id', $user_data->user_id, time()+2678400,false);
				$this->session->set_userdata($data);
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Reset password link is not valid.');
				redirect('');
			}

			$this->load->view('front/reset-password');
			$this->get_footer();
		}
		else
		{
			$user_id=$this->input->post('user_id');
			$user_type=$this->input->post('user_type');
			
			$new_pass = md5($this->input->post('confirm_password'));
			$update_data=array(
				'password'=>$new_pass,
			);
			$update_cond=array('user_id'=>$user_id);
			$updated = $this->master_model->update_record('vg_user_master',$update_data,$update_cond);

			$cond = array('user_id'=>$user_id);
			$user_data = $this->master_model->get_one_record("vg_user_master",$cond);
			$data = array(
	            'user_id' => $user_data->user_id,
				'first_name' => $user_data->first_name,
				'last_name' => $user_data->last_name,
				'user_type' => $user_data->user_type,
	       	); 

			$this->input->set_cookie('user_id', $user_data->user_id, time()+2678400,false);
			$this->session->set_userdata($data);

			$this->session->set_flashdata('success', 'Your password is successfully updated.');
			if($user_type == 'employee' || $user_type == 'support')
			{
				redirect('service-request');
			}
			else if($user_type == 'admin') 
			{
				redirect('admin');
			}
		}
	}

	public function designresetpass()
	{
		$this->get_header();
		$user_data = array();
		$this->load->view('front/reset-password',$user_data);
		$this->get_footer();
	}

	// public function designresetpassemail()
	// {
	// 	$viewData = array();
	// 	echo $this->load->view('email-templates/forgot_password', $viewData, true);
	// }

	public function randomPassword($len = 8) 
	{
	    //enforce min length 8
	    if($len < 8)
	        $len = 8;

	    //define character libraries - remove ambiguous characters like iIl|1 0oO
	    $sets = array();
	    $sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
	    $sets[] = 'abcdefghjkmnpqrstuvwxyz';
	    $sets[] = '23456789';
	    $sets[]  = '@$!%*;)?_&';

	    $password = '';
	    
	    //append a character from each set - gets first 4 characters
	    foreach ($sets as $set) {
	        $password .= $set[array_rand(str_split($set))];
	    }

	    //use all characters to fill up to $len
	    while(strlen($password) < $len) {
	        //get a random set
	        $randomSet = $sets[array_rand($sets)];
	        
	        //add a random char from the random set
	        $password .= $randomSet[array_rand(str_split($randomSet))]; 
	    }
	    
	    //shuffle the password string before returning!
	    return str_shuffle($password);
	}

}
?>