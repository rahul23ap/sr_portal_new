

<div class="container">
    <h3>Reset Password</h3>
</div>
	<div class="resetpassword-fields">
	    <form method="post" action=""  id="resetfrm">
	        <div class="form-group">
	            <label class="col-form-label">Password</label>
	            <input type="password" class="form-control" name="password" value="<?php if(!empty($_POST)){ echo set_value('password'); } ?>" id="res-password" placeholder="New Password">
	            <?php echo form_error('password'); ?>
	        </div>
	        <div class="form-group">
	            <label class="col-form-label">Confirm Password</label>
	            <input type="password" class="form-control" name="confirm_password" value="<?php if(!empty($_POST)){ echo set_value('confirm_password'); } ?>" placeholder="Confirm Password">
	            <?php echo form_error('confirm_password'); ?>
	        </div>
	        <button type="submit" class="btn btn-block pink-btn">Reset</button>
	    </form>
	</div>


<script type="text/javascript">
$(document).ready(function(){
    
    $("#resetfrm").validate({
        rules: {
            
            password:{
                required: true,
                ispassword: true,
            },
            confirm_password:{
                required: true,
                ispassword: true,
                equalTo: "#res-password",
            },
        },
        messages:{
            password:{
                required: "Please enter password.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
            },
            confirm_password:{
                required: "Please enter confirm password.",
                equalTo: "Password dose not match.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
            },
        },
        
        
        submitHandler: function(form) {
             form.submit();
        }
    });
    
         
});
</script>