<!-- Main Content -->

<div class="main-content content-with-mild-dark-bg dashboard-page">
    <div class="page-title">
        <div class="container">
            <h3>All Users</h3>
        </div>
    </div>


    <!-- Recent users table -->

    <div class="recent-users-table all-users-table">
        <div class="container">
            <?php 
                $location_id = '';
                if(!empty($_GET['location_id'])) {
                    $location_id =  $_GET['location_id'];
                }

                $sKeyword = '';
                if(!empty($_GET['sKeyword'])) {
                    $sKeyword = $_GET['sKeyword'];
                }
            ?>
            <div class="service-request-form-wrap">
                <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
                <?php } ?>
                <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
                <?php } ?>
                <form method="get">
                    <div class="service-request-search-filter-wrap">
                        <div class="service-request-form-inner">
                            <div class="service-request-form">
                                <div class="search-box">
                                    <i class="venita-search-icon"></i>
                                    <input type="text" name="sKeyword" value="<?php if(!empty($_GET['sKeyword'])) { echo $_GET['sKeyword']; } ?>" placeholder="Search by Keyword">
                                </div>
                                <div class="select-box">
                                    <select name="location_id">
                                        <option value="">Location</option>
                                        <?php
                                        foreach($location_data as $location_row)
                                        {
                                        ?> 
                                        <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($_GET['location_id'])) { if ($location_id == $location_row->location_id){ echo "selected"; } } ?>><?php echo $location_row->location_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="submit-btn">
                                <button type="submit" class="btn pink-btn">Go</button>
                            </div>
                        </div>
                        <div class="service-request-filter-wrap">
                            <?php
                                if (!empty($_GET['user_status'])) {
                                    $user_status = $_GET['user_status'];

                                    $user_status_send = implode(',', $user_status);

                                } else {
                                    $user_status = array();
                                    $user_status_send = "";
                                }
                            ?>
                            <div id="user_status" class="priprity-filter">
                            <div class="which-filter form-control">All Status</div>
                                <div class="filter-dropdown">
                                   <!--  <label class="custom-checkbox">All Status
                                        <input name="user_status[]" value="0" type="checkbox" <?php if (!empty($user_status)) {if (in_array('0', $user_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label> -->
                                    <label class="custom-checkbox">Active
                                        <input type="checkbox" name="user_status[]" value="1" <?php if (!empty($user_status)) {if (in_array('1', $user_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Inactive
                                        <input type="checkbox" name="user_status[]" value="2" <?php if (!empty($user_status)) {if (in_array('2', $user_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Delete
                                        <input type="checkbox" name="user_status[]" value="3" <?php if (!empty($user_status)) {if (in_array('3', $user_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>
                            <?php
                                if (!empty($_GET['user_type'])) {
                                    $user_type = $_GET['user_type'];
                                    $user_type_send = "'".implode ( "','", $user_type)."'";
                                } else {
                                    $user_type = array();
                                    $user_type_send = "";
                                }
                            ?>
                            <div id="user_type" class="request-filter">
                                <div class="which-request-filter form-control">All User</div>
                                <div class="filter-dropdown">
                                   <!--  <label class="custom-checkbox">All Request
                                        <input type="checkbox">
                                        <span class="custom-check"></span>
                                    </label> -->
                                    <label class="custom-checkbox">Employee
                                        <input type="checkbox" name="user_type[]" value="employee" <?php if (!empty($user_type)) {if (in_array('employee', $user_type)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Support
                                        <input type="checkbox" name="user_type[]" value="support" <?php if (!empty($user_type)) {if (in_array('support', $user_type)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Admin
                                        <input type="checkbox" name="user_type[]" value="admin" <?php if (!empty($user_type)) {if (in_array('admin', $user_type)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
            <!-- Recent Users Table -->
            <div class="table-wrap">
                <table class="common-table stripe" style="width:100%" id="viewtable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>User Id</th>
                            <th>User Name</th>
                            <th>Email Address</th>
                            <th>User Type</th>
                            <th>Location</th>
                            <th>status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready( function () {
    
    var add_class = '<?php echo $this->uri->segment(2); ?>';

    if(add_class == 'all-users')
    {
        $('body').addClass('all-users-table-page');
    }

    $(".which-filter").click(function(){
        if ($( "#user_status").is(".show-dropdown"))
        { 
            $(".priprity-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".priprity-filter").addClass("show-dropdown");
        }
    });
    $(".apply-filter").click(function(){
        $(".priprity-filter").removeClass("show-dropdown");
    });

    $('.priprity-filter').click(function(e){
        e.stopPropagation();
    });

    $('.request-filter').click(function(e){
        e.stopPropagation();
    });
    

    $(document).click(function(){
       $(".priprity-filter").removeClass("show-dropdown");
       $(".request-filter").removeClass("show-dropdown");
    });


    $(".which-request-filter").click(function(){
        if($( "#user_type").is(".show-dropdown"))
        { 
            $(".request-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".request-filter").addClass("show-dropdown");
        }

    });
    $(".apply-filter").click(function(){
        $(".request-filter").removeClass("show-dropdown");
    });

    var user_status = '<?php echo  $user_status_send; ?>';
    var user_type = "<?php echo  $user_type_send; ?>";
    var location_id = '<?php echo $location_id; ?>';
    var sKeyword = '<?php echo $sKeyword; ?>';

    var t = $("#viewtable");
    if(t.length){
        var opt = {};
        opt.responsive = true,
        opt.processing = true,
        opt.searching = false,
        opt.oLanguage= { 
            sProcessing: "", 
            sEmptyTable : "Users not found.",
            oPaginate : {
            sPrevious : "<",    
            sNext : ">",
            } 
        },
        opt.serverSide = true,
        opt.ordering = true,
        opt.bSort = false,    
        opt.info = true,
        opt.bJQueryUI= true,
        opt.lengthChange = false,
        opt.pageLength=15,
        //opt.pagingType= "input",
        opt.pagingType = "simple_numbers",
        opt.ajax = {
            url: '<?php echo base_url(); ?>admin/getalluser',
            data:{
                user_status:user_status,user_type:user_type,location_id:location_id,sKeyword:sKeyword
            },
            type:'POST',
            datatype : "application/json",
        },
        opt.columns = [
            { name : "no" },
            { name : "user_id" },
            { name: 'user_name' },
            { name: 'emailid' },
            { name: 'user_type' },
            { name: 'location_id' },
            { name: 'user_status' },
            { name: 'action' },
        ],
        opt.columnDefs = [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        opt.createdRow = function( row, data, dataIndex ) {
            $('td', row).eq(3).addClass('email-id');
            $('td', row).eq(7).addClass('action-icons');
            //$('td', row).eq(0).html(dataIndex + 1);
        },
        opt.fnRowCallback =  function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        opt.fnDrawCallback =  function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
            }
        },


        t.dataTable(opt);
        $(t).on( 'processing.dt', function ( e, settings, processing ) {
            $(t).css( 'opacity', processing ? '0.7' : '1' );
        } )
        .dataTable();
    }

    $('#viewtable').on('click','.isdelete', function (e) {
     return confirm('Are you sure you want to delete the record?');
    });
});
</script>