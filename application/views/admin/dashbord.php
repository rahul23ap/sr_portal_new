<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Main Content -->
<div class="main-content content-with-mild-dark-bg dashboard-page">
    <div class="page-title">
        <div class="container">
            <h3>Dashboard</h3>
        </div>
    </div>

    <!-- Recent users table -->

    <div class="recent-users-table">
        <div class="container">
            <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
            <?php } ?>
            <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
            <?php } ?>
            <div class="dahsboard-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="dash-box support-representatives pink-bg-shape">
                            <div class="dash-img">
                                <img src="assets/images/support-representatives.png">
                            </div>
                            <div class="dash-info">
                                <h3><?php echo $support_count; ?></h3>
                                <h5>support representatives</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="dash-box employees green-bg-shape">
                            <div class="dash-img">
                                <img src="assets/images/employees.png">
                            </div>
                            <div class="dash-info">
                                <h3><?php echo $employee_count; ?></h3>
                                <h5>employees</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="dash-box admins blue-bg-shape">
                            <div class="dash-img">
                                <img src="assets/images/admins.png">
                            </div>
                            <div class="dash-info">
                                <h3><?php echo $admin_count; ?></h3>
                                <h5>admins</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Recent Users Table -->
            <div class="table-wrap">
                <h4>recent users</h4>
                <table class="common-table stripe" style="width:100%" id="viewtable">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>User Id</td>
                            <td>User Name</td>
                            <td>Email Address</td>
                            <td>User Type</td>
                            <td>Location</td>
                            <td>status</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
$(document).ready( function () {
    
    var limit = 10;
    var t = $("#viewtable");
    if(t.length){
        var opt = {};
        opt.responsive = true,
        opt.processing = true,
        opt.searching = false,
        opt.oLanguage= {sProcessing: ""},
        opt.serverSide = true,
        opt.ordering = true,
        //opt.order = [[ 2, "asc" ]],
        opt.bSort = true,
        opt.paging = false,    
        opt.info = true,
        opt.bJQueryUI= true,
        opt.lengthChange = false,
        opt.pageLength=10,
        //opt.pagingType= "input",
        opt.pagingType = "simple",
        opt.ajax = {
            url: '<?php echo base_url(); ?>admin/getalluser',
            data:{
                limit:limit
            },
            type:'POST',
            datatype : "application/json",
        },
        opt.columns = [
            { name : "no" },
            { name : "user_id" },
            { name: 'user_name' },
            { name: 'emailid' },
            { name: 'user_type' },
            { name: 'location_id' },
            { name: 'user_status' },
            { name: 'action' },
        ],
        opt.columnDefs = [
            { 
                "targets": [ 0,-1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        opt.createdRow = function( row, data, dataIndex ) {
            $('td', row).eq(3).addClass('email-id');
            $('td', row).eq(7).addClass('action-icons');
        },
        opt.fnRowCallback =  function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        t.dataTable(opt);
        $(t).on( 'processing.dt', function ( e, settings, processing ) {
            $(t).css( 'opacity', processing ? '0.7' : '1' );
        } )
        .dataTable();
    }

    $('#viewtable').on('click','.isdelete', function (e) {
     return confirm('Are you sure you want to delete the record?');
    });
});
</script>