<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

    <!-- Main Content -->

    <?php

    $user_type = '';
    $category_ids = '';
    $service_request_ids = '';

    if(!empty($user_data) && !empty($user_data->user_type))
    {
        $user_type = $user_data->user_type;
        if(!empty($user_data->category_id) &&  $user_type == 'support')
        {
            $category_ids = $user_data->category_id;
        }

        $service_request_ids = '';
        if(!empty($user_data->service_request_id) &&  $user_type == 'support')
        {
            $service_request_ids = $user_data->service_request_id;
        }
    }   
?>

        <div class="main-content content-with-mild-dark-bg create-user--page">

            <div class="page-title">

                <div class="container">
                    <div class="back_title">
                    <h3><?php if(!empty($user_data)){ echo "Edit Profile Details"; }else{ echo "Create User"; } ?></h3>
                    <a href="<?php echo base_url('admin/all-users'); ?>" class="back-button"><i class="venita-long-back-arrow"></i>Back</a>
                </div>
                </div>

            </div>

            <!-- Create User -->

            <div class="create-user-wrap">
                
                <form method="post" action="" id="createuserfrm" enctype="multipart/form-data">
                    <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                    <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
                    <?php } ?>
                    <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
                    <?php } ?>
                    <div class="create-user-box">

                        <?php if(!empty($user_data)){ ?>

                            <h4 class="username"><?php echo ucfirst($user_data->first_name).' '.ucfirst($user_data->last_name); ?> <span>#<?php echo 'VG'.$user_data->user_id; ?></span></h4>

                            <?php } ?>

                                <h4>Basic Details</h4>

                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="col-form-label">First Name<span>*</span></label>

                                            <input type="text" class="form-control" name="first_name" value="<?php if(!empty($_POST)){ echo set_value('first_name');}elseif(!empty($user_data)) { echo $user_data->first_name; } ?>" placeholder="Enter First Name">

                                            <?php echo form_error('first_name'); ?>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="col-form-label">Last Name<span>*</span></label>

                                            <input type="text" class="form-control" name="last_name" value="<?php if(!empty($_POST)){ echo set_value('last_name');}elseif(!empty($user_data)) { echo $user_data->last_name; } ?>" placeholder="Enter Last Name">

                                            <?php echo form_error('last_name'); ?>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="col-form-label">Username<span>*</span></label>

                                            <input type="hidden" value="<?php if(!empty($user_data->user_name)){ echo $user_data->user_name; } ?>" id="originaluser_name" />

                                            <input type="text" class="form-control" name="user_name" value="<?php if(!empty($_POST)){ echo set_value('user_name');}elseif(!empty($user_data)) { echo $user_data->user_name; } ?>" <?php if(!empty($user_data) && !empty($user_data->user_name)){ echo "readonly"; }?> id="user_name" placeholder="Enter Username">

                                            <?php echo form_error('last_name'); ?>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="col-form-label">Password<span>*</span></label>

                                            <div class="inp-toggle-eye">

                                                <input type="password" class="form-control password-span" name="password" placeholder="********" autocomplete="off">

                                                <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password"></span>

                                                <?php echo form_error('password'); ?>
                                                <input type="hidden" name="hid_password" value="<?php if(!empty($user_data->password)){ echo $user_data->password; } ?>">
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label class="col-form-label">Email Address<span>*</span></label>

                                            <input type="hidden" value="<?php if(!empty($user_data->emailid)){ echo $user_data->emailid; } ?>" id="originalemailid" />

                                            <input type="email" class="form-control" name="emailid" value="<?php if(!empty($_POST)){ echo set_value('emailid');}elseif(!empty($user_data)) { echo $user_data->emailid; } ?>" placeholder="Enter Email Address">

                                            <?php echo form_error('emailid'); ?>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="col-form-label">Select Location<span>*</span></label>

                                            <select class="form-control" name="location_id">

                                                <option value="">Select Location</option>

                                                <?php
                                                    foreach($location_data as $location_row)
                                                    {
                                                ?>
                                                  <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($_POST)) { if (set_value( 'location_id')==$location_row->location_id){ echo "selected"; } }elseif(!empty($user_data)) { if($user_data->location_id == $location_row->location_id) {echo "selected";} } ?>><?php echo $location_row->location_name;?></option>
                                                    <?php } ?>

                                            </select>

                                            <?php echo form_error('location_id'); ?>

                                        </div>

                                    </div>

                                </div>

                                <div class="additional-details-box">

                                    <div class="additional-details-accordion">

                                        <h4>Additional Details</h4>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <label class="col-form-label">Type of User<span>*</span></label>

                                                <select class="form-control" name="user_type" id="user_type">

                                                    <option value="">Select type of user</option>

                                                    <option value="employee" <?php if(!empty($_POST)){if(set_value( 'user_type')=='employee' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_type =="employee") echo 'selected="selected"'; } ?>>Employee</option>

                                                    <option value="support" <?php if(!empty($_POST)){if(set_value( 'user_type')=='support' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_type =="support") echo 'selected="selected"'; } ?>>Support</option>

                                                    <option value="admin" <?php if(!empty($_POST)){if(set_value( 'user_type')=='admin' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_type =="admin") echo 'selected="selected"'; } ?>>Admin</option>

                                                </select>

                                                <?php echo form_error('user_type'); ?>

                                            </div>

                                        </div>

                                        <div class="clear"></div>

                                        <div id="hideshow" class="col-md-12" style="display:none;">
                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label class="col-form-label">Category<span>*</span></label>

                                                        <select class="form-control multiselect-box" name="category_id[]" id="category_id" multiple="multiple">

                                                            <?php

                                        foreach($category_data as $category_row)

                                        {

                                        ?>

                                                                <option value="<?php echo $category_row->category_id;?>" <?php if(!empty($_POST)) { if (set_value( 'category_id')==$category_row->category_id){ echo "selected"; } }elseif(!empty($user_data)) { if($user_data->category_id == $category_row->category_id) {echo "selected";} } ?>>
                                                                    <?php echo $category_row->category_name;?>
                                                                </option>

                                                                <?php } ?>

                                                        </select>

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label class="col-form-label">Service Type<span>*</span></label>

                                                        <select class="form-control multiselect-box" name="service_request_id[]" id="service_request_id" multiple="multiple">

                                                            <?php

                                        if(!empty($user_data))

                                        {

                                            foreach($service_request_data as $service_request_row)

                                            {

                                        ?>

                                                                <option value="<?php echo $service_request_row->service_request_id;?>">
                                                                    <?php echo $service_request_row->service_request_name;?>
                                                                </option>

                                                                <?php } } ?>

                                                        </select>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <?php 

                        if(!empty($user_data))

                        { ?>

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label class="col-form-label">Status<span>*</span></label>

                                                    <select class="form-control" name="user_status" id="user_status">

                                                        <option value="">Select status</option>

                                                        <option value="1" <?php if(!empty($_POST)){if(set_value( 'user_status')=='1' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_status =="1") echo 'selected="selected"'; } ?>>Active User</option>
                                                        <option value="2" <?php if(!empty($_POST)){if(set_value( 'user_status')=='2' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_status =="2") echo 'selected="selected"'; } ?>>Inactive User</option>
                                                        <option value="3" <?php if(!empty($_POST)){if(set_value( 'user_status')=='3' )echo 'selected';}elseif(!empty($user_data)) { if($user_data->user_status =="3") echo 'selected="selected"'; } ?>>Delete User</option>

                                                    </select>

                                                </div>

                                            </div>

                                            <?php }

                    ?>

                                    </div>

                                </div>

                                <div class="submit-button">

                                    <button type="submit" class="btn btn-block pink-btn">
                                        <?php if(!empty($user_data)){ echo "Update"; }else{ echo "Create Account"; } ?>
                                    </button>
                                    <a href="<?php echo base_url('admin/all-users'); ?>" class="cancel-btn">Cancel</a>

                                </div>

                    </div>

                </form>

            </div>

        </div>

<script type="text/javascript">
$(document).ready(function() {

        // Get All Category
        var getall_cat = '<?php echo $category_ids; ?>';
        var vals = JSON.parse("[" + getall_cat + "]");
        var category_id = '';
        if (getall_cat != '')
        {
            $('#category_id').val(vals).trigger("change");

            category_id = $("#category_id").val();

            get_sr_by_category_id(category_id);
        }

        // Get All Service Type

        // var getall_service_request_id = '<?php //echo $service_request_ids; ?>';
        // var vals = JSON.parse("[" + getall_service_request_id + "]");

        // if (getall_service_request_id != '')
        // {
        //     $('#service_request_id').val(vals).trigger("change");
        // }

        var user_type = '<?php echo $user_type; ?>';
        if (user_type != '' && user_type == 'support')
        {
            $("#hideshow").show();
        }

        $('#user_type').bind('change', function() {
            var user_type = $('#user_type').find(":selected").val();
            if (user_type == 'support')
            {
                $("#hideshow").show();
            } 
            else
            {
                $("#hideshow").hide();
            }
        });

        $('#category_id').on('select2:select, select2:unselect, select2:close', function(e) {
            var category_id = $("#category_id").select2("val");
            if (category_id && category_id.length > 0)
            {
                $("#service_request_id").html('');
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url();?>admin/get_service_request',
                    data: {
                        category_id: category_id
                    },
                    dataType: "json",
                    success: function(res) {
                        if (res.result == "success") {
                            if (res.data != '')
                            {
                                $("#service_request_id").html(res.data);

                            }
                        }
                    }
                });
            } 
            else
            {
                $("#service_request_id").html('');
            }
        });

function get_sr_by_category_id(category_id)
{
    if (category_id && category_id.length > 0)
    {
       // $("#service_request_id").html('');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>admin/get_service_request',
            data: {
                category_id: category_id
            },
            dataType: "json",
            success: function(res) {
                if (res.result == "success") {
                    if (res.data != '')
                    {
                        $("#service_request_id").html(res.data);
                        var getall_service_request_id = '<?php echo $service_request_ids; ?>';
                        var vals = JSON.parse("[" + getall_service_request_id + "]");

                        if (getall_service_request_id != '')
                        {
                            $('#service_request_id').val(vals).trigger("change");
                        }
                    }
                }
            }
        });
    } 
    else
    {
        $("#service_request_id").html('');
    }
}

   $("#user_name").on('keyup', function() {
        var el = document.getElementById("user_name");
        var val = el.value.replace(/\s/g, "");
        $("#user_name").val(val);
    });

    $("#user_name").on('keypress', function() {
        var el = document.getElementById("user_name");
        var val = el.value.replace(/\s/g, "");
        $("#user_name").val(val);
    }); 



        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "");

        $("#createuserfrm").validate({
            rules: {
                first_name: {
                    required: true,
                    isstring: true,
                },
                last_name: {
                    required: true,
                    isstring: true,
                },
                user_name: {
                    required: true,
                    noSpace: true,
                    remote: {
                        param: {
                            url: "<?php echo base_url(); ?>admin/geteusername",
                            type: "post"
                        },
                        depends: function(element) {
                            // compare email address in form to hidden field
                            return ($(element).val() !== $('#originaluser_name').val());
                        }
                    },
                },
                <?php if(empty($user_data) && empty($user_data->password)){ ?>
                password: {
                    required: true,
                    ispassword: true,
                },
                <?php }else{ ?>
                password: {
                    ispassword: true,
                },    
                <?php } ?>
                emailid: {
                    required: true,
                    isemail: true,
                    remote: {
                        param: {
                            url: "<?php echo base_url(); ?>admin/getemailexists",
                            type: "post"
                        },
                        depends: function(element) {
                            // compare email address in form to hidden field
                            return ($(element).val() !== $('#originalemailid').val());
                        }
                    },
                },
                location_id: {
                    required: true,
                },
                user_type: {
                    required: true,
                },
                <?php if(empty($user_data) && empty($user_data->password)){ ?>
                user_status: {
                    required: true,
                },
                <?php } ?>
                'category_id[]': {
                    required: function() {
                        var user_type = $('#user_type').find(":selected").val();
                        if ((user_type == 'support')) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                },
                'service_request_id[]': {
                    required: function() {
                        var user_type = $('#user_type').find(":selected").val();
                        if ((user_type == 'support')) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                },
            },
            messages: {
                first_name: {
                    required: 'Please enter first name.',
                    isstring: 'Please enter valid first name.',
                },
                last_name: {
                    required: "Please enter last name.",
                    isstring: 'Please enter valid last name.',
                },
                user_name: {
                    required: "Please enter user name.",
                    noSpace: "Please enter valid username without space.",
                    remote: "User name is already exists.",
                },
                password: {
                    required: "Please enter password.",
                    ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
                },
                emailid: {
                    required: "Please enter email address.",
                    isemail: "Please enter valid email address.",
                    remote: "Email address is already exists.",
                },
                location_id: {
                    required: "Please select location.",
                },
                user_type: {
                    required: "Please select user type.",
                },
                user_status: {
                    required: "Please select status.",
                },
                'category_id[]': {
                    required: "Please select category type.",
                },
                'service_request_id[]': {
                    required: "Please select service request type.",
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("id") == "category_id" || element.attr("id") == "service_request_id") {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            },
        });
    });
</script>

<script type="text/javascript">
$(function() {
    var current = '<?php echo base_url('admin/create-user') ?>';
    $(".main-menu ul li").removeClass("active");
    $(".main-menu ul li a[href='" + current + "']").parent("li").addClass("active");
    jQuery(".main-menu ul li a[href='" + current + "']").parent("li").parent("ul").parent("div").parent("li").addClass("active");
});
</script>