<?php 
    $CI =& get_instance();
    $user_id = $this->session->userdata('user_id');
    $user_type = $this->session->userdata('user_type');
?>
<!-- Main Content -->

    <div class="main-content content-with-mild-dark-bg service-request-details-page">
        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3><?php if(!empty($sr_data)){  echo $sr_data->sr_title; }  ?> - <span>#VG<?php if(!empty($sr_data)){  echo $sr_data->sr_id; }  ?></span></h3>
                    </div>
                    <div class="col-md-4">
                        <div class="service-requests-action-buttons">
                            <?php 
                                if(!empty($sr_data->sr_status) && $sr_data->sr_status != 'Deleted')
                                { ?>
                            <div class="edit-delete-buttons">
                                <?php 
                                if(!empty($sr_data->sr_status) && $sr_data->sr_status == 'Closed'){
                                ?>
                                <a href="#" class="reopen-button" data-toggle="modal" data-target="#deleteRequestmodal"><i class="venita-reopen-icon"></i>reopen</a>
                                <?php }else{ ?>
                                <a href="#" class="edit-button" data-toggle="modal" data-target="#editSRmodal"><i class="venita-edit-icon"></i>edit</a>    
                                <?php } ?>
                                <a href="#" class="delete-button" data-toggle="modal" data-target="#deleteRequestmodal"><i class="venita-delete-icon"></i>delete</a>
                            </div>
                            <?php } ?>
                            <a href="<?php echo  base_url('service-request'); ?>" class="back-button"><i class="venita-long-back-arrow"></i>Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Details Section -->

        <div class="service-request-details">
            <div class="container">
                <?php if(!empty($this->session->flashdata('success'))){ ?>
                    <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success') ?></div>
                <script type="text/javascript">
                    $(document).ready(function($) {
                      $("#chatid").animate({ scrollTop: $('#chatid').prop("scrollHeight")}, 2000);
                    });
                </script>
                <?php } ?>
                <div class="succesdiv"></div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="card-box description-block">
                            <h5>Description</h5>
                            <div class="desctiption-info">
                                <?php if(!empty($sr_data)){  echo $sr_data->sr_description; }  ?>
                            </div>
                            <div class="attachments">
                                <?php 
                                    if(!empty($sr_attachment)){  ?>
                                <h6>Attachment</h6>
                                <div class="attachments-box">
                                    <?php 
                                        foreach ($sr_attachment as $sr_attachment_row) { 

                                            $url = base_url('uploads/sr_documents/' . $sr_attachment_row->sattachment);
                                            ?>
                                            <span><a href="<?php echo $url ?>" target="_blank"><?php echo $sr_attachment_row->sattachment; ?></a></span>
                                       <?php }
                                    ?>   
                                </div>
                                <?php }
                                    ?>
                            </div>
                        </div>
                        <?php 
                            if(!empty($sr_data->sr_status) && $sr_data->sr_status != 'Deleted')
                            { ?>
                        <?php 
                            if(empty($message_data))
                            { ?>
                        <div class="submit-buttons">
                            <button type="button" id="replyshow" class="btn pink-btn">Reply</button>
                        </div>
                        <?php } ?>
                         <?php 
                            if(!empty($message_data))
                            {
                                
                             ?>
                        <div class="card-box chat-box" id="chatid">
                            <div class="chat-inner">
                                <?php 
                                    $previous_date = null;
                                    foreach ($message_data as $message_data_row) {
                                    
                                    $class = '';
                                    if(($user_id == $message_data_row->sender_id && $user_type == 'employee') && $message_data_row->parent_message_id == 0)
                                    {
                                        $class = 'sent';
                                    }
                                    elseif(($user_id != $message_data_row->sender_id && $user_type == 'employee') && $message_data_row->parent_message_id == 0)
                                    {
                                        $class = 'received';
                                    
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    }
                                    elseif(($user_id == $message_data_row->sender_id || $user_type == $message_data_row->sender_role) && $message_data_row->parent_message_id == 0)
                                    {
                                        $class = 'sent';
                                    }
                                    elseif(($user_id != $message_data_row->sender_id || $user_type == $message_data_row->sender_role) && $message_data_row->parent_message_id == 0)
                                    {
                                        $class = 'received';
                                    
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    }
                                    elseif(($user_id != $message_data_row->sender_id && $user_type != $message_data_row->sender_role) && $message_data_row->parent_message_id != 0)
                                    {
                                        $class = 'received replied-to-above';
                                    
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    }
                                    elseif(($user_id != $message_data_row->sender_id || ($user_type == $message_data_row->sender_role || $user_type != $message_data_row->sender_role)) && $message_data_row->parent_message_id != 0)
                                    {
                                        $class = 'sent replied-to-above';
                                    
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    }
                                    
                                    // elseif(($user_id != $message_data_row->sender_id || $user_type != $message_data_row->sender_role) && $message_data_row->parent_message_id != 0)
                                    // {
                                    //     $class = 'sent replied-to-above';
                                    
                                    //     $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    // }
                                    // elseif($user_id == $message_data_row->sender_id && $message_data_row->parent_message_id != 0)
                                    // {
                                    //     $class = 'sent replied-to-above';
                                    
                                    //     $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    // }
                                    

                                    $date = date('m-d-Y',strtotime($message_data_row->dCreatedDate));

                                    if ($date != $previous_date) 
                                    {
                                        $previous_date = $date;
                                        echo '<div class="chat-date-span"><i class="venita-calender-icon"></i>'.$date.'</div>';
                                    }

                                    $get_msg_attachment =  $CI->get_msg_attachment($message_data_row->message_id);
                                    $attachment_div = '';
                                    if(!empty($get_msg_attachment)){ 
                                    $attachment_div  .= '<div class="description-block attachments">
                                                <div class="attachments-box">';
                                        foreach ($get_msg_attachment as $sr_attachment_row) { 

                                            $url = base_url('uploads/sr_message_doc/'.$sr_attachment_row->sattachment);
                                           
                                            $attachment_div  .= '<span><a href="'.$url.'" target="_blank">'.$sr_attachment_row->sattachment.'</a></span>';
                                       }
                                        $attachment_div  .= '</div></div>';
                                    }
                                    
                                
                                if($class == 'sent')
                                { 
                                ?>
                                <div class="<?php echo $class; ?>">
                                    <p><?php echo $message_data_row->message; ?></p>
                                    <span class="time"><?php echo date('h:ia',strtotime($message_data_row->dCreatedDate)); ?></span>
                                    <?php echo $attachment_div;?>
                                </div>
                                <?php 
                                }
                                elseif($class == 'received'){ 
                                ?>
                                    <div class="<?php echo $class; ?>">
                                        <span class="username"><?php echo $user_name; ?></span>
                                        <div class="received-message">
                                            <p><?php echo $message_data_row->message; ?></p>
                                            <a class="reply_this" data-id="<?php echo $message_data_row->message_id; ?>" data-message="<?php echo $message_data_row->message; ?>" data-username="<?php echo $user_name; ?>"><i class="venita-reply-icon"></i></a>
                                            <span class="time"><?php echo date('h:ia',strtotime($message_data_row->dCreatedDate)); ?></span>
                                        <?php echo $attachment_div;?>
                                        </div>
                                    </div>

                                <?php }
                                elseif($class == 'sent replied-to-above')
                                {
                                    if($user_id != $message_data_row->sender_id || $user_type == $message_data_row->sender_role)
                                    {
                                        //$class="sent replied-to-above";
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                        $reply_msg =  $CI->get_reply_msg($message_data_row->parent_message_id);
                                    }
                                    // elseif($user_id == $message_data_row->sender_id)
                                    // {
                                    //     $class="sent replied-to-above";
                                    //     $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    //     $reply_msg =  $CI->get_reply_msg($message_data_row->parent_message_id);
                                    // }
                                ?>

                                <div class="<?php echo $class; ?>">
                                    <div class="above-text">
                                        <span class="username"><?php echo $user_name; ?></span>
                                        <p><?php echo $reply_msg; ?></p>
                                    </div>
                                    <p><?php echo $message_data_row->message; ?></p>
                                     <span class="time"><?php echo date('h:ia',strtotime($message_data_row->dCreatedDate)); ?></span>
                                     <?php echo $attachment_div;?>
                                </div>

                               <?php } 
                               elseif($class == 'received replied-to-above')
                                {
                                    if($user_id != $message_data_row->sender_id || $user_type == $message_data_row->sender_role)
                                    {
                                        //$class="sent replied-to-above";
                                        $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                        $reply_msg =  $CI->get_reply_msg($message_data_row->parent_message_id);
                                    }
                                    // elseif($user_id == $message_data_row->sender_id)
                                    // {
                                    //     $class="sent replied-to-above";
                                    //     $user_name =  $CI->get_assign_name($message_data_row->sender_id);
                                    //     $reply_msg =  $CI->get_reply_msg($message_data_row->parent_message_id);
                                    // }
                                ?>

                                <div class="<?php echo $class; ?>">
                                    <div class="above-text">
                                        <span class="username"><?php echo $user_name; ?></span>
                                        <p><?php echo $reply_msg; ?></p>
                                    </div>
                                    <p><?php echo $message_data_row->message; ?></p>
                                     <span class="time"><?php echo date('h:ia',strtotime($message_data_row->dCreatedDate)); ?></span>
                                     <?php echo $attachment_div;?>
                                </div>

                               <?php }   
                                ?>

                            <?php } ?>
                            </div>
                        </div>
                        <?php } 
                        if(!empty($sr_data->sr_status) && $sr_data->sr_status != 'Closed'){
                        ?>
                        <div class="card-box reply-box" id="hideshow" style="<?php if(empty($message_data)){ echo 'display: none;';}else{ echo "display: block;";  } ?> ">
                            <div class="title"><i class="venita-reply-icon"></i>Reply</div>
                            <form id="msgfrm" method="post" action="<?php echo base_url('service_request/msg_send'); ?>" enctype="multipart/form-data">
                                <div id="reply-title" class="reply-title"></div>
                                <div class="type-reply">
                                    <div id="reply-to-message" class=""></div>
                                    <textarea placeholder="Comment" name="message"></textarea>
                                </div>
                                <div class="form-with-add-button">
                                    <div class="form-group with-tooltip">
                                        <div class="attach-file-input">
                                            <div class="input-files-wrap">
                                                <span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span>
                                                <input type="file" class="form-control attachment_cls" name="attachment_file[]">
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" id="addButton_reply" class="more_btn"><i class="fas fa-plus-circle"></i>Add More</a>
                                </div>
                            <div id="adddynamic" class="">                             
                            </div>  
                                <input type="hidden" name="sr_id" value="<?php if(!empty($sr_data->sr_id)){ echo $sr_data->sr_id; } ?>">
                                <button type="submit" class="btn pink-btn">Submit</button>
                            </form>
                        </div>
                    <?php } } ?>
                    </div>
                    <div class="col-md-4">
                        <div class="service-request-details--info menu-accordion">
                            <div class="details-info-title open-accordion">
                                <div class="title">Service Request Details</div>
                                <div class="name">
                                    <div class="title-span">Assign To </div><span class="name-span"><?php if(!empty($sr_data->assign_id)) { echo  $CI->get_assign_name($sr_data->assign_id); }else{ echo "Not assigned"; } ?></span>
                                </div>
                            </div>
                            <div class="details-info-list">
                                <div class="details-info-individual name">
                                    <span class="title-span">name</span><span class="name-span"><?php if(!empty($sr_data->name)) { echo $sr_data->name;} ?></span>
                                </div>
                                <div class="details-info-individual email">
                                    <span class="title-span">email</span><span class="name-span"><a href="mailto:<?php if(!empty($sr_data->emailid)) { echo $sr_data->emailid;} ?>"><?php if(!empty($sr_data->emailid)) { echo $sr_data->emailid;} ?></a></span>
                                </div>
                                <div class="details-info-individual category">
                                    <span class="title-span">category</span><span class="name-span"><?php if(!empty($sr_data->category_id)) { echo $CI->get_category($sr_data->category_id); } ?></span>
                                </div>
                                <div class="details-info-individual type">
                                    <span class="title-span">SR type</span><span class="name-span"><?php if(!empty($sr_data->service_request_id)) { echo $CI->get_srtype($sr_data->service_request_id); } ?></span>
                                </div>
                                <div class="details-info-individual location">
                                    <span class="title-span">location</span><span class="name-span"><?php if(!empty($sr_data->location_id)) { echo $CI->get_location($sr_data->location_id); } ?></span>
                                </div>                              
                                <div class="details-info-individual status">
                                    <span class="title-span">status</span><span class="name-span">
                                        <!-- 3 color codes (blue-color, red-color, green color) -->    
                                    <select class="blue-color" name="sr_status" id="sr_status_update" <?php if(!empty($sr_data->sr_status) && ($sr_data->sr_status == 'Closed' || $sr_data->sr_status == 'Deleted')){ echo "disabled"; } ?>>
                                        <option value="Open" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Open') {echo 'selected';}}?>>Open</option>
                                        <option value="InProgress" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'InProgress') {echo 'selected';}}?>>In Progress</option>            
                                        <option value="Closed" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Closed') {echo 'selected';}}?>>Closed</option>
                                    </select>
                                        
                                    </span>
                                </div>
                                <?php 
                                if(!empty($sr_data->reason)  && ($sr_data->sr_status == 'Closed' || $sr_data->sr_status == 'Delete'))
                                    {
                                ?>
                                <div class="details-info-individual name">
                                    <span class="title-span">reason</span><span class="name-span"><?php if(!empty($sr_data->reason)) { echo $sr_data->reason;} ?></span>
                                </div>
                                <?php } ?>
                                <?php 
                                    $class= "";
                                    if($sr_data->priority == 'High')
                                    {
                                        $class= "high-priority";
                                    }
                                    elseif ($sr_data->priority == 'Low') {
                                        $class= "low-priority";
                                    }
                                    elseif ($sr_data->priority == 'Medium') {
                                         $class= "medium-priority";
                                    }
                                ?>
                                <div class="details-info-individual priority">
                                    <span class="title-span">Priority</span><span class="name-span <?php echo $class; ?>"><?php if(!empty($sr_data->priority)){ echo $sr_data->priority; } ?></span>
                                </div>
                                <div class="details-info-individual due-date">
                                    <span class="title-span">due date</span><span class="name-span"><?php if(!empty($sr_data->due_date)) { echo date("m-d-Y",strtotime($sr_data->due_date)); } ?></span>
                                </div>
                                <div class="details-info-individual requested-on">
                                    <span class="title-span">requested on</span><span class="name-span"><?php if(!empty($sr_data->dCreatedDate)) { echo date("m-d-Y",strtotime($sr_data->dCreatedDate)); } ?></span>
                                </div>
                            </div>
                        </div>

                        <?php 
                        if(!empty($sr_data->sr_status) && $sr_data->sr_status != 'Deleted')
                            {
                        if($user_type == 'support') { 
                            $assign_data = $CI->support_list($sr_data->sr_id);
                        ?>
                            <form method="post" action="<?php echo base_url('service_request/set_assign_id') ?>">
                            <div class="succesdiv_assign"></div>
                            <div class="assign_to_box">
                                <select name="assign_id" id="assign_id1" class="btn btn-block blue-btn" onchange="this.form.submit()">
                                    <option value="">Assign To</option>
                                    <?php foreach ($assign_data as $assign_data_row) { ?>
                                        <option value="<?php echo $assign_data_row->user_id; ?>" ><?php echo $assign_data_row->first_name.' '.$assign_data_row->last_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <input type="hidden" name="sr_id" value="<?php if(!empty($sr_data->sr_id)) { echo $sr_data->sr_id; }  ?>">
                            <input type="hidden" name="sr_status" value="<?php if(!empty($sr_data->sr_status)) { echo $sr_data->sr_status; }  ?>">
                            </form>
                        <?php } }?>

                        <div class="service-request-history menu-accordion" id="sr_history">
                            <div class="history-title open-accordion">request history</div>
                            <div class="history-details" id="sr_history_id">
                                <?php 
                                    if(!empty($request_history))
                                    {
                                        foreach ($request_history as $request_history_row)
                                        {
                                ?>
                                <div class="history-individual">
                                    <div class="history-title"><?php echo $request_history_row->activity_type; ?><?php if(strpos($request_history_row->activity_type, 'By') !== false || strpos($request_history_row->activity_type, 'To') !== false)
                                        { 
                                            $activity_name =  $CI->get_assign_name($request_history_row->activity_by);
                                            echo ' <span class="blue-text">'.$activity_name.'</span>'; } ?></div>
                                    <div class="history-info">
                                        <div class="date-n-time">on <?php echo date('m-d-Y, h:ia',strtotime($request_history_row->dCreatedDate)); ?></div>
                                        <div class="status">status: <span class="blue-text"><?php echo $request_history_row->sr_status; ?></span></div>
                                    </div>
                                </div>
                                <?php  }
                                    } 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /Main Content -->

    <!--- Edit SR --->
    <div class="modal fade edit-SR-modal" id="editSRmodal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            Edit My Service Request
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="venita-times-icon"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url('service_request/updatesr'); ?>" id="editsr" enctype="multipart/form-data">
                <div class="edit-service-request-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Name<span>*</span></label>
                                <input type="text" name="name" value="<?php if(!empty($sr_data) && !empty($sr_data->name)) { echo $sr_data->name; } ?>" <?php if(!empty($sr_data) && !empty($sr_data->name)) { echo "readonly"; } ?> class="form-control"> 
                                <?php echo form_error('name'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Email Address<span>*</span></label>
                                <input type="email" class="form-control" name="emailid" value="<?php if(!empty($sr_data) && !empty($sr_data->emailid)) { echo $sr_data->emailid; } ?>" <?php if(!empty($sr_data) && !empty($sr_data->emailid)) { echo "readonly"; } ?> placeholder="Enter Email Address">
                                <?php echo form_error('emailid'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Service Request Title</label>
                                <input type="text" name="sr_title" value="<?php if(!empty($sr_data->sr_title)) { echo $sr_data->sr_title; } ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Service Request Description</label>
                                <textarea name="sr_description"><?php if(!empty($sr_data->sr_description)) { echo $sr_data->sr_description; } ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Location</label>
                                <select class="form-control" name="location_id">
                                    <option value="">Select Location</option>
                                    <?php
                                    if(!empty($location_data))
                                    {
                                        foreach($location_data as $location_row)
                                        {
                                    ?>
                                    <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($sr_data->location_id)) { if($sr_data->location_id == $location_row->location_id) {echo "selected";} } ?>><?php echo $location_row->location_name;?></option>
                                    <?php } 
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Requester<span>*</span></label>
                                <select class="form-control" name="requester_type" <?php if(!empty($sr_data->requester_type)) { echo "disabled"; } ?>>
                                    <option value="employee" <?php if(!empty($sr_data) && $sr_data->requester_type =='employee') { echo "selected"; } ?>>Employee</option>
                                    <option value="support" <?php if(!empty($sr_data) && $sr_data->requester_type=='support') { echo "selected"; } ?>>Support</option>
                                    <option value="admin" <?php if(!empty($sr_data) && $sr_data->requester_type=='admin') { echo "selected"; } ?>>Admin</option>
                                </select>
                                <input type="hidden" name="requester_type_hid" value="<?php if(!empty($sr_data->requester_type)) { echo $sr_data->requester_type; } ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Category<span>*</span></label>
                                <select class="form-control"  <?php if($user_type == 'employee'){ echo "disabled"; } ?> name="category_id" id="category_id">
                                    <?php
                                    foreach($category_data as $category_row)
                                    {
                                    ?>
                                    <option value="<?php echo $category_row->category_id;?>" <?php if(count($category_data) == 1 && ($category_row->category_id == 1)){ echo "selected"; }elseif($sr_data->category_id  == $category_row->category_id){ echo "selected"; } ?>><?php echo $category_row->category_name;?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="category_id_hid" id="category_id_hid">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Service Request Type</label>
                                <select class="form-control" name="service_request_id" id="service_request_id">
                                    <option value="">Select Service Request Type</option>
                                <?php 
                                    if(!empty($sr_data->category_id)){ 

                                    $cond=array('category_id'=>$sr_data->category_id,'isActive' =>1,'isDelete'=>0);
        
                                     $service_request_data = $this->master_model->get_record('vg_service_request',$cond);

                                    foreach($service_request_data as $service_request_row)
                                    {
                                ?>
                                    <option value="<?php echo $service_request_row->service_request_id;?>" <?php if(!empty($sr_data->service_request_id)) { if($sr_data->service_request_id == $service_request_row->service_request_id) {echo "selected";} } ?>><?php echo $service_request_row->service_request_name;?></option>
                                <?php }  }?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Priority</label>
                            <select class="form-control" name="priority">
                                <option value="High" <?php if (!empty($sr_data->priority)) { if ($sr_data->priority == 'Open') {echo 'selected';}}?> >High</option>
                                <option value="Medium" <?php if (!empty($sr_data->priority)) { if ($sr_data->priority == 'Open') {echo 'selected';}}?> >Medium</option>
                                <option value="Low" <?php if (!empty($sr_data->priority)) { if ($sr_data->priority == 'Open') {echo 'selected';}}?>>Low</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Status</label>
                            <select class="form-control" name="sr_status" <?php if (!empty($sr_data->sr_status)) { echo "disabled"; } ?>>
                                <option value="Open" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Open') {echo 'selected';}}?>>Open</option>
                                <option value="InProgress"<?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'InProgress') {echo 'selected';}}?>>InProgress</option>            
                                <option value="Closed" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Closed') {echo 'selected';}}?>>Closed</option>
                            </select>
                            <input type="hidden" name="sr_status_hid" value="<?php if (!empty($sr_data->sr_status)) { echo $sr_data->sr_status; } ?>">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Due Date</label>
                            <input type="text" class="form-control ui-datepicker-inp calender-icon" id="due_date" name="due_date" value="<?php  if (!empty($sr_data->due_date)) {  echo date('m/d/Y',strtotime($sr_data->due_date)); } ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 description-block ">
                        <div class="attachments">
                                <?php 
                                    if(!empty($sr_attachment)){  ?>
                                <label class="col-form-label">Uploaded Attachment</label>
                                <div class="attachments-box row">
                                    <?php 
                                        foreach ($sr_attachment as $sr_attachment_row) { 

                                            $url = base_url('uploads/sr_documents/' . $sr_attachment_row->sattachment);
                                            ?>
                                            <div class="col-md-6"><span><a href="<?php echo $url ?>" target="_blank"><?php echo $sr_attachment_row->sattachment; ?></a></span></div>
                                       <?php }
                                    ?>   
                                </div>
                                <?php }
                                    ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-with-add-button">
                                <div class="form-group with-tooltip">
                                    <label class="col-form-label">Attachment</label>
                                    <div class="attach-file-input">
                                        <div class="input-files-wrap">
                                            <span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span>
                                            <input type="file" class="form-control attachment_cls" name="attachment_file[]">
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" id="addButtonedit" class="pop-more-button"><i class="fas fa-plus-circle"></i>Add More</a>
                            </div>
                        </div>
                        <div id="adddynamic_edit" class="col-md-12">                             
                        </div>
                    </div>

                    
                    <div class="submit-buttons">
                        <input type="hidden" name="sr_id" value="<?php  if(!empty($sr_data->sr_id)){ echo $sr_data->sr_id; } ?>">
                        <button type="submit" class="btn pink-btn">Update</button>
                        <a href="javascript:void(0);" class="cancel-button" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--- Edit SR END --->


    <!--- Delete Popup --->
    <div class="modal fade delete-SR-modal" id="deleteRequestmodal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            Confirmation
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="venita-times-icon"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <p id="confirm_text">Are you sure want to delete this Service Request?</p>
            <div class="yes-no-btns">
                <a href="#" id="yes_id" class="btn pink-btn" data-toggle="modal" data-target="#deleteRequestmodalDetails" data-dismiss="modal">Yes</a>
                <a href="#" class="btn blue-btn" data-toggle="modal" data-dismiss="modal">No</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--- Delete End --->

    <!---- Delete Confirm ---->
    <div class="modal fade delete-SR-modal-details" id="deleteRequestmodalDetails" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <span id="popup_line">Submit details to confirm service request deletion</span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="venita-times-icon"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <form id="deletefrm" method="post" action="<?php echo base_url('service_request/updatesrstatus'); ?>">
                <div class="form-group">
                    <label class="col-form-label">Status</label>
                    <select class="form-control" name="sr_status" <?php if (!empty($sr_data->sr_status)) { echo "disabled"; } ?>>
                        <option value="Open" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Open') {echo 'selected';}}?>>Open</option>
                        <option value="InProgress"<?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'InProgress') {echo 'selected';}}?>>InProgress</option>            
                        <option value="Closed" <?php if (!empty($sr_data->sr_status)) { if ($sr_data->sr_status == 'Closed') {echo 'selected';}}?>>Closed</option>
                    </select>
                    <input type="hidden" name="sr_status_hid_pop" id="sr_status_hid_pop" value="Deleted">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Reason<span>*</span></label>
                    <select class="form-control" name="reason">
                        <option value="">Select Valid Reason</option>
                        <option value="Resone 1">Resone 1</option>
                        <option value="Resone 2">Resone 2</option>
                        <option value="Resone 3">Resone 3</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Comment</label>
                    <textarea name="comment" placeholder="Enter Comment"></textarea>
                </div>
                <button type="submit" class="btn pink-btn">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript">
$(document).ready(function($) {
    $("#chatid").animate({ scrollTop: $('#chatid').prop("scrollHeight")}, 2000);
    $('.reply_this').click(function(e) {
        $('.remove_message').remove();
        var message_id = $(this).data("id");
        var message = $(this).data("message");
        var username = $(this).data("username");
        $('#reply-to-message').addClass('reply-to-message');
        $('.reply-title').html(username);
        $('.reply-to-message').html(message);
        $('#msgfrm').append('<input type="hidden" value="'+message_id+'" class="remove_message" name="message_id">');
        //console.log(message_id+message);
    });

    $('#assign_id').change(function(e) {
        var assign_id = $("#assign_id").val();
        if(assign_id !='')
        {
            var sr_id = '<?php echo $sr_data->sr_id; ?>';
            var sr_status = '<?php echo $sr_data->sr_status; ?>';
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>service_request/set_assign_id',
                data: {
                    assign_id: assign_id,sr_id:sr_id,sr_status:sr_status
                },
                dataType: "json",
                success: function(res) {
                    if (res.result == "success") {
                        if (res.data != '')
                        {
                            $(".succesdiv_assign").html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Service request assigned successfully.</div>');
                            $("#assign_id").html(res.data);
                            $("#sr_history").load(" #sr_history > *");
                        }
                    }
                }
            });
        }
    });


    $('#sr_status_update').change(function(e) { 
        var sr_status = $("#sr_status_update").val();
        var sr_id = '<?php echo $this->uri->segment(2); ?>';
        var from_ajax = 'Yes';
        if(sr_status != 'Closed')
        {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>service_request/updatesrstatus',
                data: {
                    sr_status: sr_status,sr_id:sr_id,from_ajax:from_ajax
                },
                dataType: "json",
                success: function(res) {
                     if(res.result == "success"){
                        if(res.status_change == "true")
                        {
                            $('.succesdiv').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Status updated successfully.</div>');
                            $("#sr_history").load(" #sr_history > *");
                        }
                    }
                }
            });
        }
        else if(sr_status == 'Closed')
        {
            $('#confirm_text').html('Are you sure want to close this Service Request?');
            $('#popup_line').html('Submit details to confirm service request closing');
            $('#sr_status_hid_pop').val('Closed');
            $('#deleteRequestmodal').modal();
        }
    });


    $('.reopen-button').click(function(e) {
        $('#confirm_text').html('Are you sure want to reopen this Service Request?');
        $('#popup_line').html('Submit details to confirm service request reopening');
        $('#sr_status_hid_pop').val('Open');
        $('#deletefrm').append('<input type="hidden" class="sr_status_reopen" value="Reopen" name="sr_status_reopen">');
    });

    $('.delete-button').click(function(e) {
        $('#confirm_text').html('Are you sure want to delete this Service Request?');
        $('#popup_line').html('Submit details to confirm service request deletion');
        $('#sr_status_hid_pop').val('Deleted');
    });

    $('#yes_id').click(function(e) {
        $('.remove_sr_id').remove();
        var sr_id = '<?php echo $this->uri->segment(2); ?>';
        $('#deletefrm').append('<input type="hidden" value="'+sr_id+'" class="remove_sr_id" name="sr_id">');
    });

    $('#deleteRequestmodalDetails').on('hidden.bs.modal', function () {
        $('.remove_sr_id').remove();
         $('.sr_status_reopen').remove();
    });


    $("#due_date").datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0,
    });


    var category_id ='';
    category_id = $("#category_id").val();
    if(category_id !='')
    {
        $('#category_id_hid').val(category_id);
        //get_service_request(category_id);
    }

    $('#category_id').bind('change', function() {
        category_id = $("#category_id").val();
        get_service_request(category_id);
    });

    function get_service_request(category_id)
    {
        if (category_id !='' && category_id.length > 0)
        {
            $("#service_request_id").html('');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>front_master/get_service_request',
                data: {
                    category_id: category_id
                },
                dataType: "json",
                success: function(res) {
                    if (res.result == "success") {
                        if (res.data != '')
                        {
                            $("#service_request_id").html(res.data);
                        }
                    }
                }
            });
        } 
        else
        {
            $("#service_request_id").html('');
        }
    }


    $('#replyshow').bind('click', function() {
        $("#hideshow").toggle();
    });

    
    // $(".input-files-wrap").on('click','.minus',function(){
    //     $(this).prev("input.attachment_cls").remove();
    //     $(this).remove();
    // });

    // $('.add-more-button123').click(function() {
    //     var structure = $('<div class="input-new-file-attach"><input type="file" name="attachment_file[]" class="form-control attachment_cls"><a class="minus"><i class="fas fa-minus-circle"></i></a></div>');
    //    $('.input-files-wrap').append(structure);
    // });

     $.validator.addMethod('filesize', function (value, element, param) {
    //console.log(element.files[0].size/1024/1024);
    return this.optional(element) || (element.files[0].size <= param)
    }, 'Please upload file less than 5MB.');

     $("#msgfrm").validate({
        rules: 
        {
            message:
            {
                required:true,
            },
        },
        messages: 
        {
            message:
            {
                required:"Please enter comment.",
            },
        },
    }); 

    //Message reply
     $(document).on('click','.minus_btn',function(){
        var removeid=$(this).attr('iditem');
        $("#ReplyBoxDiv" + removeid).remove();
        $(".clear").remove();
        counter--;
    });

    var counter = 2;
            
    $("#addButton_reply").click(function () {
        
        var x = Math.floor((Math.random() * 9999) + 1);

        var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", 'ReplyBoxDiv' + counter).addClass("divfileupload");
        
        newTextBoxDiv.after().html( 
        '<div class="form-with-add-button"><div class="form-group with-tooltip"><div class="attach-file-input"><div class="input-files-wrap"><span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span><input type="file" class="form-control attachment_cls"  name="attachment_file['+counter+']"></div></div></div><a class="minus_btn more_btn" iditem='+counter+' id="removeButton'+ counter +'"><i class="fas fa-minus-circle"></i></a></div>');
        
        
        newTextBoxDiv.appendTo("#adddynamic");
       
        //$('<div class="clear"></div>').insertAfter('#TextBoxDiv' + counter);
        $("input.attachment_cls").each(function(){
            $(this).rules("add", {
                extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
                filesize: 5242880,
                messages: {
                   extension: "Please upload valid file type.",
                }
            });                   
        });

        counter++;
    });


   

    $("#editsr").validate({
        rules: {
            location_id:{
                required: true,
            },
            priority:{
                required: true,
            },
            due_date:{
                required: true,
            },
            sr_title:{
                required: true,
            },
            sr_description:{
                required: true,
            },
            sr_status:{
                required: true,
            },
            service_request_id: {
                required : true,
            },
            
        },
        messages:{
            location_id:{
                required: "Please select location.",
            },
            priority:{
                required: "Please select priority.",
            },
            due_date:{
                required: "Please select due date.",
            },
            sr_title:{
                required: "Please enter title.",
            },
            sr_description:{
                required: "Please enter description.",
            },
            sr_status:{
                required: "Please select status.",
            },
            service_request_id: {
                required : "Please select service request type.",
            },
        },
        errorPlacement: function(error, element) {
              error.insertAfter(element);
        },
    });

    $("input.attachment_cls").each(function(){
        $(this).rules("add", {
            extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
            filesize: 5242880,
            messages: {
               extension: "Please upload valid file type.",
            }
        });                   
    });

    // Edit Popup add more
    $(document).on('click','.minus',function(){
        var removeid=$(this).attr('iditem');
        $("#FileBoxDiv" + removeid).remove();
        $(".clear").remove();
        counter--;
    });

    var counter = 2;
            
    $("#addButtonedit").click(function () {
        
        var x = Math.floor((Math.random() * 9999) + 1);

        var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", 'FileBoxDiv' + counter).addClass("divfileupload");
        
        newTextBoxDiv.after().html( 
        '<div class="form-with-add-button"><div class="form-group with-tooltip"><div class="attach-file-input"><div class="input-files-wrap"><span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span><input type="file" class="form-control attachment_cls"  name="attachment_file['+counter+']"></div></div></div><a class="minus more_btn" iditem='+counter+' id="removeButton'+ counter +'"><i class="fas fa-minus-circle"></i></a></div>');
        
        
        newTextBoxDiv.appendTo("#adddynamic_edit");
       
        $("input.attachment_cls").each(function(){
            $(this).rules("add", {
                extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
                filesize: 5242880,
                messages: {
                   extension: "Please upload valid file type.",
                }
            });                   
        });
        //$('<div class="clear"></div>').insertAfter('#TextBoxDiv' + counter);

       

        counter++;
    });


    // responsive
    
     $('.open-accordion').click(function() {
       $(this).parent().toggleClass('show-items'); 
    });


    $("#deletefrm").validate({
        rules: 
        {
            reason:
            {
                required:true,
            },
            // comment:
            // {
            //     required:true,
            // },
        },
        messages: 
        {
            reason:
            {
                required: "Please select reason.",
            },
            // comment:
            // {
            //     required:"Please enter comment.",
            // },
        },
    }); 

    


    });
</script>
<script type="text/javascript">
$(function() {
    var current = '<?php echo base_url('service-request') ?>';
    $(".main-menu ul li").removeClass("active");
    $(".main-menu ul li a[href='" + current + "']").parent("li").addClass("active");
    jQuery(".main-menu ul li a[href='" + current + "']").parent("li").parent("ul").parent("div").parent("li").addClass("active");
});
</script>