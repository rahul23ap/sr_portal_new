<!-- Main Content -->
<?php 
$user_type = $this->session->userdata('user_type');
$CI =& get_instance(); 

// $category_ids = '';
// if(!empty($category_data))
// {
//     foreach ($category_data as $category_data_row) {
//         $category_id[] = $category_data_row->category_id;
//     }

//     $category_ids = implode(',', $category_id);

// }
 ?>


<div class="main-content content-with-mild-dark-bg create-service-request-page">
    <div class="page-title title-center">
        <div class="container">
            <h3>Create New SR</h3>
        </div>
    </div>
    <!-- Create SR -->
    <div class="create-service-request-box-wrap">
        <form id="createrequestfrm" method="post" action="" enctype="multipart/form-data">
            <div class="create-service-request-box">
            <div class="row">
                <div class="col-md-6">
                    <?php
                        if($user_type == 'employee')
                        {
                    ?>
                    <div class="form-group">
                        <label class="col-form-label">Name<span>*</span></label>
                        <input type="text" name="name" value="<?php if(!empty($user_data) && !empty($user_data[0]->first_name)) { echo $user_data[0]->first_name.' '.$user_data[0]->last_name; } ?>" <?php if(!empty($user_data) && !empty($user_data[0]->first_name)) { echo "readonly"; } ?> class="form-control"> 
                        <?php echo form_error('name'); ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label class="col-form-label">Select Name<span>*</span></label>
                        <select class="form-control" name="name" id="select_name">
                            <option value="" data-id="">Select name</option>
                            <?php
                            $employee_data =  $CI->get_employee_user();
                            foreach($employee_data as $employee_data_row)
                            {
                            ?>
                            <option value="<?php echo $employee_data_row->first_name.' '.$employee_data_row->last_name;?>" data-id="<?php echo $employee_data_row->user_id ?>"><?php echo $employee_data_row->first_name.' '.$employee_data_row->last_name;?></option>
                            <?php } ?>
                            <input type="hidden" name="user_id_hid" id="user_id_hid">
                        </select>
                    </div>
                <?php } ?>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Email Address<span>*</span></label>
                        <?php
                        if($user_type == 'employee')
                        {
                        ?>
                        <input type="email" class="form-control" name="emailid"  value="<?php if(!empty($user_data) && !empty($user_data[0]->emailid)) { echo $user_data[0]->emailid; } ?>" <?php if(!empty($user_data) && !empty($user_data[0]->emailid)) { echo "readonly"; } ?> placeholder="Enter Email Address">
                    <?php }else{ ?>
                        <input type="email" class="form-control" name="emailid" id="dynamic_email" readonly placeholder="Enter Email Address">
                        <?php echo form_error('emailid'); ?>
                    <?php } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Category<span>*</span></label>
                        <select class="form-control" name="category_id" id="category_id">
                            <?php
                            foreach($category_data as $category_row)
                            {
                            ?>
                            <option value="<?php echo $category_row->category_id;?>"><?php echo $category_row->category_name;?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="category_id_hid" id="category_id_hid">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Type<span>*</span></label>
                        <select class="form-control" name="service_request_id" id="service_request_id">

                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Select Location<span>*</span></label>
                        <select class="form-control" name="location_id" id="location_id">
                            <option value="">Select Location</option>
                            <?php
                                foreach($location_data as $location_row){
                            ?>
                            <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($user_data) && $user_type == 'employee' && !empty($user_data[0]->location_id) && $user_data[0]->location_id== $location_row->location_id){ echo "selected"; }elseif(!empty($_POST)) { if (set_value( 'location_id')==$location_row->location_id){ echo "selected"; }} ?>><?php echo $location_row->location_name;?></option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Requester<span>*</span></label>
                        <select class="form-control" name="user_type" <?php if(!empty($user_type)) { echo "disabled"; } ?>>
                            <option value="employee" <?php if(!empty($_POST)){if(set_value( 'user_type')=='employee' )echo 'selected';}elseif(!empty($user_type) && $user_type=='employee') { echo "selected"; } ?>>Employee</option>
                            <option value="support" <?php if(!empty($_POST)){if(set_value( 'user_type')=='support' )echo 'selected';}elseif(!empty($user_type) && $user_type=='support') { echo "selected"; } ?>>Support</option>
                            <option value="admin" <?php if(!empty($_POST)){if(set_value( 'user_type')=='admin' )echo 'selected';}elseif(!empty($user_type) && $user_type=='admin') { echo "selected"; } ?>>Admin</option>
                        </select>
                        <input type="hidden" name="user_type_hid" value="<?php if(!empty($user_type)) { echo $user_type; } ?>">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-form-label">Service Request Title<span>*</span></label>
                        <input type="text" class="form-control" name="sr_title" placeholder="Please enter service request title">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Priority <?php if($user_type == 'support'){ echo "<span>*</span>"; } ?></label>
                        <select class="form-control" name="priority" id="priority" <?php if($user_type == 'employee'){ echo "disabled"; } ?>>
                            <option value="">Please Select priority</option>
                            <option value="High">High</option>
                            <option value="Low">Low</option>
                            <option value="Medium">Medium</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Need by Date</label>
                        <input type="text" class="form-control ui-datepicker-inp calender-icon" name="due_date" id="due_date" placeholder="MM-DD-YYYY" readonly="" <?php if($user_type == 'employee'){ echo "disabled"; } ?>>
                    </div>
                </div>
                <?php if($user_type =='support'){ ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Assigned To</label>
                        <select class="form-control" name="assign_id" id="assign_id">
                            <option value="">Select Assign To</option>   
                        </select>
                    </div>
                </div>
                <div class="clear"></div>
                <?php } ?>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-form-label">Service Request Description<span>*</span></div>
                        <textarea name="sr_description" placeholder="Please Enter Service Request Description"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                        <div class="form-with-add-button">
                            <div class="form-group with-tooltip">
                                <label class="col-form-label">Attachment</label>
                                <div class="attach-file-input">
                                    <div class="input-files-wrap">
                                        <span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span>
                                        <input type="file" class="form-control attachment_cls" name="attachment_file[]">
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" id="addButton" class="add-more-button"><i class="fas fa-plus-circle"></i>Add More</a>
                        </div>
                    </div>
                <div id="adddynamic" class="col-md-12">                             
                </div>
            </div>
            <div class="submit-buttons">
                <button type="submit" class="btn pink-btn">Submit</button>
                <a href="<?php echo base_url('service-request'); ?>" class="cancel-btn">Cancel</a>
            </div>
            </div>
        </form>
    </div>
</div>
<!-- /Main Content -->
<script type="text/javascript">
$(document).ready(function($) {

    $("#due_date").datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0,
    });

    $.validator.addMethod('filesize', function (value, element, param) {
    //console.log(element.files[0].size/1024/1024);
    return this.optional(element) || (element.files[0].size <= param)
    }, 'Please upload file less than 5MB.');

     $("#createrequestfrm").validate({
        ignore: [],
        errorClass: "error",
        errorElement: "div",
        rules: {
            name:{
                required: true,
            },
            emailid:{
                required: true,
                isemail:true,
            },
            location_id:{
                required: true,
            },
            user_type:{
                required: true,
            },
            priority:{
                required: true,
            },
            // due_date:{
            //     required: true,
            // },
            sr_title:{
                required: true,
            },
            sr_description:{
                required: true,
            },
            category_id: {
                required : true,
            },
            service_request_id: {
                required : true,
            },
            // 'attachment_file[]':{
            //     //required :true,
            //     extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
            //     filesize: 5242880
            // },
            
        },
        messages:{
            name:{
                required: 'Please enter first name.',
                isstring: 'Please enter valid first name.',
            },
            emailid:{
                required: "Please enter email address.",
                isemail: "Please enter valid email address.",
                remote: "Email address is already exists.",
            },
            location_id:{
                required: "Please select location.",
            },
            user_type:{
                required: "Please select user type.",
            },
            priority:{
                required: "Please select priority.",
            },
            due_date:{
                required: "Please select need by date.",
            },
            sr_title:{
                required: "Please enter title.",
            },
            sr_description:{
                required: "Please enter description.",
            },
            category_id: {
                required : "Please select category type.",
            },
            service_request_id: {
                required : "Please select service request type.",
            },
            'attachment_file[]':{
                //required : "Please select document.",
                extension: "Please upload valid file type.",
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("class") == "attachment_cls"){
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        highlight: function(element, errorClass)
        {
            $('input').removeClass('error');
        },
        // submitHandler: function (form)
        // {
        //     if($("#createrequestfrm").valid())
        //     {
        //         console.log('hey');
        //         //form.submit();
        //     }
        // }
    });

    

    $("input.attachment_cls").each(function(){
        $(this).rules("add", {
            extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
            filesize: 5242880,
            messages: {
               extension: "Please upload valid file type.",
            }
        });                   
    });

    // var getall_cat = '<?php //echo $category_ids; ?>';
    // var vals = JSON.parse("[" + getall_cat + "]");

    // if (getall_cat != '')
    // {
    //     $('#category_id').val(vals).trigger("change");
    // }

    $("#select_name").change(function () {
        var user_id = $(this).find(':selected').data('id');
        if(user_id !='')
        {
            $("#user_id_hid").val(user_id);
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>service_request/get_email_id_ajax',
                data: {
                    user_id: user_id
                },
                dataType: "json",
                success: function(res) {
                    if (res.result == "success") {
                        if (res.data != '')
                        {
                            $("#dynamic_email").val(res.data.emailid);
                            $("#location_id option[value='"+res.data.location_id+"']").prop('selected', true);
                        }
                    }
                }
            }); 
        }
        else
        {
            $("#dynamic_email").val('');
            $("#user_id_hid").val(user_id);
        }
    }); 


    $('#service_request_id').bind('change', function() {
        var categoryid = $("#category_id").val();
        var service_request_id = $("#service_request_id").val();

        if(categoryid !='' && service_request_id !='')
        {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>service_request/support_user',
                data: {
                    category_id: categoryid,service_request_id:service_request_id
                },
                dataType: "json",
                success: function(res) {
                    if (res.result == "success") {
                        if (res.data != '')
                        {
                            $("#assign_id").html(res.data);
                        }
                    }
                }
            }); 
        }
    });


    var category_id ='';
    category_id = $("#category_id").val();
    if(category_id !='')
    {
        $('#category_id_hid').val(category_id);
        get_service_request(category_id);
    }

    $('#category_id').bind('change', function() {
        category_id = $("#category_id").val();
        $('#category_id_hid').val(category_id);
        get_service_request(category_id);
    });

function get_service_request(category_id)
{
    if (category_id !='' && category_id.length > 0)
    {
        $("#service_request_id").html('');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>front_master/get_service_request',
            data: {
                category_id: category_id
            },
            dataType: "json",
            success: function(res) {
                if (res.result == "success") {
                    if (res.data != '')
                    {
                        $("#service_request_id").html(res.data);
                    }
                }
            }
        });
    } 
    else
    {
        $("#service_request_id").html('');
    }
}



    //  Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('click','.minus',function(){
        var removeid=$(this).attr('iditem');
        $("#FileBoxDiv" + removeid).remove();
        $(".clear").remove();
        counter--;
    });

    var counter = 2;
            
    $("#addButton").click(function () {
        
        var x = Math.floor((Math.random() * 9999) + 1);

        var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", 'FileBoxDiv' + counter).addClass("divfileupload");
        
        newTextBoxDiv.after().html( 
        '<div class="form-with-add-button"><div class="form-group with-tooltip"><div class="attach-file-input"><div class="input-files-wrap"><span class="tooltip-mark" data-toggle="tooltip" data-placement="top" title="Attach a file to elaborate about the Service Request"><i class="fas fa-question-circle"></i></span><input type="file" class="form-control attachment_cls"  name="attachment_file['+counter+']"></div></div></div><a class="minus" iditem='+counter+' id="removeButton'+ counter +'"><i class="fas fa-minus-circle"></i></a></div>');
        
        
        newTextBoxDiv.appendTo("#adddynamic");
       
        //$('<div class="clear"></div>').insertAfter('#TextBoxDiv' + counter);

        $("input.attachment_cls").each(function(){
            $(this).rules("add", {
                extension: "png|jpe?g|pdf|doc|docx|txt|xls|csv|xlsx",
                filesize: 5242880,
                messages: {
                    extension: "Please upload valid file type.",
                }
            });                   
        });

        counter++;
    });

});

</script>