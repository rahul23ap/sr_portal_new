<?php 
    $CI =& get_instance();
?>

<style type="text/css">
    .disp-none {
        display: none;
    }
</style>
<!-- Main Content -->

<div class="main-content content-with-mild-dark-bg">
    <div class="page-title">
        <div class="container">
            <h3>My Service Requests</h3>
        </div>
    </div>

    <!-- Service Request listing Form -->
     <?php 
        $location_id = '';
        if(!empty($_GET['location_id'])) {
            $location_id =  $_GET['location_id'];
        }

        $sKeyword = '';
        if(!empty($_GET['sKeyword'])) {
            $sKeyword = $_GET['sKeyword'];
        }
    ?>
    <div class="service-request-listing-wrap">
        <div class="container">
            <div class="service-request-form-wrap">
                <form method="get">
                    <div class="service-request-search-filter-wrap">
                        <div class="service-request-form-inner">
                            <div class="service-request-form">
                                <div class="search-box">
                                    <i class="venita-search-icon"></i>
                                    <input type="text" name="sKeyword" value="<?php if(!empty($_GET['sKeyword'])) { echo $_GET['sKeyword']; } ?>" placeholder="Search by Title or Assigned To">
                                </div>
                                <div class="select-box">
                                        <select name="location_id">
                                        <option value="">Location</option>
                                        <?php
                                        foreach($location_data as $location_row)
                                        {
                                        ?> 
                                        <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($_GET['location_id'])) { if ($location_id == $location_row->location_id){ echo "selected"; } } ?>><?php echo $location_row->location_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="date-box">
                                    <input type="text" class="ui-datepicker-inp" name="request_date" placeholder="Select Requested Date">
                                </div>
                            </div>
                            <div class="submit-btn">
                                <button type="submit" class="btn pink-btn">Go</button>
                            </div>
                        </div>
                        <?php
                            if (!empty($_GET['priority'])) {
                                $priority = $_GET['priority'];
                                $priority_send = "'".implode ( "','", $priority)."'";
                            } else {
                                $priority = array();
                                $priority_send = "";
                            }
                        ?>
                        <div class="service-request-filter-wrap">
                            <div  id="priority" class="priprity-filter">
                                <div class="which-filter form-control">All Priority</div>
                                <div class="filter-dropdown">
                                    <label class="custom-checkbox">All Priority
                                        <input type="checkbox" name="priority[]" value="All" <?php if (!empty($priority)) {if (in_array('All', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Low Priority
                                        <input type="checkbox" name="priority[]" value="Low" <?php if (!empty($priority)) {if (in_array('Low', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Medium Priority
                                        <input type="checkbox" name="priority[]" value="Medium" <?php if (!empty($priority)) {if (in_array('Medium', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">High Priority
                                        <input type="checkbox" name="priority[]" value="High" <?php if (!empty($priority)) {if (in_array('High', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>
                            <?php
                                if (!empty($_GET['sr_status'])) {
                                    $sr_status = $_GET['sr_status'];
                                    $sr_status_send = "'".implode ( "','", $sr_status)."'";
                                }else {
                                    $sr_status = array();
                                    $sr_status_send = "";
                                }
                        ?>
                            <div id="sr_status" class="request-filter">
                                <div class="which-request-filter form-control">All Request</div>
                                <div class="filter-dropdown">
                                    <label class="custom-checkbox">All Request
                                        <input type="checkbox" name="sr_status[]" value="All" <?php if (!empty($sr_status)) {if (in_array('All', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Open
                                        <input type="checkbox" name="sr_status[]" value="Open" <?php if (!empty($sr_status)) {if (in_array('Open', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Closed
                                        <input type="checkbox" name="sr_status[]" value="Closed" <?php if (!empty($sr_status)) {if (in_array('Closed', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Deleted
                                        <input type="checkbox" name="sr_status[]" value="Deleted" <?php if (!empty($sr_status)) {if (in_array('Deleted', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Service Request listing -->

    <div class="service-request-listing">
        <div class="container">
             <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
            <?php } ?>
            <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
            <?php } ?>
            <div class="request-listing">
                <?php
                    if (count($sr_data) > 0) {
                ?>
                 <table id="DataTable">
                    <thead class="disp-none">
                        <tr>
                        <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 

                        foreach ($sr_data as $sr_data_row) {
                        
                            $location_name = $CI->get_location($sr_data_row->location_id);

                            $class= "";
                            if($sr_data_row->priority == 'High')
                            {
                                $class= "high-priority";
                            }
                            elseif ($sr_data_row->priority == 'Low') {
                                $class= "low-priority";
                            }
                            elseif ($sr_data_row->priority == 'Medium') {
                                 $class= "medium-priority";
                            }

                        ?>
                        <tr>
                            <td>    
                                <div class="request-individual">
                                    <div class="request-title"><?php echo $sr_data_row->sr_title; ?> - <span>#VG<?php echo  $sr_data_row->sr_id; ?></span></div>
                                    <div class="requset-to-wrap">
                                        <div class="request-to">
                                            <div class="request-item">
                                                <div class="small-title">assigned to</div>
                                                <span class="small-title-detail">james hannes</span>
                                            </div>
                                            <div class="request-item">
                                                <div class="small-title">due date</div>
                                                <span class="small-title-detail"><?php echo date("m-d-Y",strtotime($sr_data_row->due_date)); ?></span>
                                            </div>
                                            <div class="request-item">
                                                <div class="small-title">status</div>
                                                <span class="small-title-detail"><?php echo $sr_data_row->sr_status; ?></span>
                                            </div>
                                            <div class="request-item">
                                                <div class="small-title">location</div>
                                                <span class="small-title-detail"><?php echo $location_name; ?></span>
                                            </div>
                                            <div class="request-item">
                                                <div class="small-title">priority</div>
                                                <span class="small-title-detail <?php echo $class; ?>"><?php echo $sr_data_row->priority; ?></span>
                                            </div>
                                        </div>
                                        <div class="request-actions">
                                            <div class="action-buttons">
                                                <a href="#" class="reopen-button"><i class="venita-reopen-icon"></i>Reopen</a>
                                                <div class="view-details-button">
                                                    <a href="#" class="btn pink-outline-btn">view details</a>
                                                </div>
                                            </div>
                                            <div class="requested-on-date">requested on: <span><?php echo date("m-d-Y",strtotime($sr_data_row->dCreatedDate));?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php }else {
                echo '<div class="alert alert-warning">Request Not Found!</div>';
            } ?>
            </div>
        </div>
    </div>
</div>
<!-- /Main Content -->
<script type="text/javascript">
$(document).ready(function() {
   var DataTable=$('#DataTable').DataTable({
        "paging": true,
        "pagingType": "simple_numbers",
        "language" :  {
            paginate: {
           next: ">",
           previous: "<"
            }
        },
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false ,
        "pageLength": 10,
        "fnDrawCallback" :  function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
            }
        },
    });
    DataTable.on('page.dt', function() {
        $('html, body').animate({
            scrollTop: $(".dataTables_wrapper").offset().top-70
        }, 'slow');
    });
});
</script>
<script>
    $( function() {
        $(".ui-datepicker-inp").datepicker();
    });

    $(".which-filter").click(function(){
        if ($( "#priority").is(".show-dropdown"))
        { 
            $(".priprity-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".priprity-filter").addClass("show-dropdown");
        }
    });
    $(".apply-filter").click(function(){
        $(".priprity-filter").removeClass("show-dropdown");
    });

    $('.priprity-filter').click(function(e){
        e.stopPropagation();
    });

    $('.request-filter').click(function(e){
        e.stopPropagation();
    });
    

    $(document).click(function(){
       $(".priprity-filter").removeClass("show-dropdown");
       $(".request-filter").removeClass("show-dropdown");
    });


    $(".which-request-filter").click(function(){
        if($( "#sr_status").is(".show-dropdown"))
        { 
            $(".request-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".request-filter").addClass("show-dropdown");
        }

    });
    $(".apply-filter").click(function(){
        $(".request-filter").removeClass("show-dropdown");
    });


    $(window).load(function() {
    var viewportWidth = $(window).width();
        if (viewportWidth < 768) {
            $(".request-individual").addClass("mobile-view-accordion");

            $(".request-title").click(function() {
                $(this).parent().toggleClass("mobile-view-accordion");
            });
        }
    });
</script>