<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Master_model extends CI_Model
{
	function __construct()
	{
        parent::__construct();
	}


	public function validate_user($table,$cond)
	{
        $query = $this->db->get_where($table,$cond);
        //echo $this->db->last_query();
		return $query->row();
    }

    public function insert_record($table,$data)
	{
		//print_r ($data);
       	$this->db->insert($table,$data);
	   	return $this->db->insert_id();
	}

    public function get_count($table,$cond)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($cond);
		$query=$this->db->get();
		//echo $this->db->last_query();
		return $query->num_rows();
    }
	public function get_one_record($table,$cond)
	{
		$query = $this->db->get_where($table,$cond);
		return $query->row();
    }
    public function get_record($table,$cond)
	{
		$query = $this->db->get_where($table,$cond);
		//echo $this->db->last_query();
		return $query->result();
    }
    public function get_record_cust($table,$cond,$select='*',$having='')
	{
		$this->db->select($select);
		if(!empty($having))$this->db->having($having);
		$query = $this->db->get_where($table,$cond);
		//echo $this->db->last_query();
		return $query->result();
    }
    public function get_record_fields($table,$fields,$cond,$order="")
	{
		$this->db->select($fields);
		$this->db->from($table);
		$this->db->where($cond);
		$this->db->order_by($order);
		$query=$this->db->get();
		return $query->result();
    }
    public function get_record_with_join($table1,$table2,$common1,$common2,$cond,$select='*',$order="")
	{
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->join("$table2", "$table1.$common1 = $table2.$common2",'inner');

		$query = $this->db->where($cond);
		if(!empty($order))$this->db->order_by($order);		
		$query=$this->db->get();
		//echo $this->db->last_query();
		return $query->result();
    }
    public function get_record_with_pagination($table,$cond,$limit, $start,$order)
	{
		$this->db->limit($limit, $start);
		$query = $this->db->get_where($table,$cond);
		return $query->result();
    } 
	public function update_record($table,$data,$cond)
	{
		$this->db->where($cond);
		$this->db->update($table, $data);
	}
	public function delete_record($table,$cond)
	{
		$this->db->where($cond);
		$this->db->delete($table);
	}
	public function get_record_limit($table,$cond,$limit="",$order="")
	{
		$this->db->from($table);
		$this->db->where($cond);
		$this->db->limit($limit);
		$this->db->order_by($order);
		$query=$this->db->get();
		return $query->result();
    }
    
    public function custom_query($query)
    {
    	$res = $this->db->query($query)->result();
    	//echo $this->db->last_query();
    	return $res;
    }
}
?>