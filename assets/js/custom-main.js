$(document).ready(function(){

    jQuery.validator.addMethod("isemail", function(value, element) {
        //return this.optional(element) || /^[a-zA-Z0-9+._-]+@[a-zA-Z0-9-]+\.[a-zA-Z-]{2,4}$/.test(value);
        return this.optional(element) || /^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(value);
    }, "");
    
    jQuery.validator.addMethod("isstring", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    }, "");

    jQuery.validator.addMethod("ispassword", function(value, element) {
        return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*;)?_&])[A-Za-z\d@$!%*;)?_&]{8,}$/.test(value);
    }, "");

    $(".forgot-password-btn").click(function(){
        $(".login-forgot-password-box").addClass("show-forgot-password");
    });
    $(".back-button").click(function(){
        $(".login-forgot-password-box").removeClass("show-forgot-password");
    });

    $('.multiselect-box').select2();
});

$(document).on('click', '.toggle-password', function() {
	$(this).toggleClass("fa-eye fa-eye-slash");
	var input = $(".password-span");
	input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
/********************      FOOTER HEIGHT            ****************/
jQuery(window).on('load resize', function(){
    var header_height = jQuery("header").outerHeight();
    var footer_height = jQuery("footer").outerHeight();
    var breadcomes_height = jQuery(".main-content").outerHeight();
    var window_height = jQuery(window).outerHeight();
    var head_foot_height = header_height + footer_height;
    var tot_height = window_height - head_foot_height;
    jQuery('.dashboard-page').css('min-height', tot_height);
    jQuery('.main-content + div').css('min-height', tot_height - breadcomes_height);
});
jQuery(window).on('load resize', function(){
$(".menu_toggle").click(function(){
  $(".nav_menu").slideToggle();
});
});